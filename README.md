## Personal Development Tracking

Platform to help software engineers to grow as a professionals. This SaaS solution makes it possible for leads, managers to flexibly adjust definition of seniority level in scope of the company. It contains lots of paths to grow for each competency in terms of backend, frontent, etc. Apart from that it is possible to create a personal development plan for the employee which helps to make a peformance review and track its progress online. There are different roles to separate administrators from usual users.

Solution is implemented with following of all best practices and clean code principles.

## Technologies

* .NET Core 3.1
* ASP .NET Core 3.1
* Entity Framework Core 3.1
* MediatR
* AutoMapper
* FluentValidation
* NUnit, FluentAssertions
* Serilog

## Architecture Overview
Platform arhictecture is based on **Onion** architecture that defines layers from the core to the Infrastructure. Apart from that there is **Command-Query Segregation (CQS)** pattern is used implemented via MediatR package.

![Onion Architecture](/.media/onion.png "Onion Architecture")

### Domain

Domain layer represents the business and behavior objects. The idea is to have all of your domain objects at this core. It holds all application domain objects. Besides the domain objects, you also could have domain interfaces. These domain entities don't have any dependencies. This will contain all entities, enums, exceptions, interfaces, types and logic specific to the domain layer.

### Application

This layer contains all application logic. It is dependent on the domain layer, but has no dependencies on any other layer or project. This layer defines interfaces that are implemented by outside layers.

### Infrastructure

This layer contains classes for accessing external resources such as file systems, web services, smtp, and so on. These classes should be based on interfaces defined within the application layer.

### WebAspNetCore

It's the outer-most layer, it represents the Web API. This layer has an implementation of the dependency injection principle so that the application builds a loosely coupled structure and can communicate to the internal layer via interfaces.

## Security

There is an [Auth0](https://auth0.com/) is used for authentication as an Identity Provider. Implicit grant type makes it possible to authenticate SPA applications. There is no direct dependencies to the Auth0 and it supports every OAuth 2.0 Identity providers.

## Configuration

There is a combination of configuration providers is used to setup the project. First of it is *appsettings.json* files where regular parameters are stored and depending on environment different json files could be used (eg. *appsettings.Development.json*). All sensistive parameters should be injected via environment variables while deployment or for development environment there is user secrets could be used.
It is **Options Pattern** used for working with configuration.

## Logging

Logging is implemented with using *Serilog*. According to best practices code should not have dependency to whatever logging framework is choosen. So that *ILooger* from *Microsoft.Extensions.Logging* is using for logging. And *Serilog* setup the destination of logs in *Startup.cs* based on configuration. By default, we use Database to write logs to. It could be easily changed on the configuration level where to write logs without any impact on code base.