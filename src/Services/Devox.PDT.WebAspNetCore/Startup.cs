using AutoMapper;
using Devox.PDT.Application.Infrastructure;
using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Infrastructure.Authentication.Middleware;
using Devox.PDT.Infrastructure.Authentication.OAuth0;
using Devox.PDT.Infrastructure.Authorization;
using Devox.PDT.Infrastructure.CurrentUser;
using Devox.PDT.Infrastructure.Data.Extensions;
using Devox.PDT.Infrastructure.ExceptionHandling.Middleware;
using Devox.PDT.Infrastructure.Swagger.Extensions;
using Devox.PDT.Infrastructure.Swagger.Options;
using Devox.PDT.Persistance;
using MediatR;
using MediatR.Extensions.FluentValidation.AspNetCore;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Devox.PDT.WebAspNetCore
{
    public class Startup
    {
        private readonly IWebHostEnvironment currentEnvironment;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            currentEnvironment = env;
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();
            services.AddDbContext<PdtDbContext>(
                options =>
                options.UseSqlServer(Configuration.GetConnectionString("Devox.PDT")), ServiceLifetime.Transient);

            services.AddMemoryCache();

            //Add MediatR
            var domainAssembly = typeof(SpecializationModel).GetTypeInfo().Assembly;
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));
            services.AddMediatR(domainAssembly);
            services.AddFluentValidation(new[] { domainAssembly });


            services.AddApiVersioning(
            options =>
            {
                // specify the header[s] containing the api versions
                options.ApiVersionReader = new HeaderApiVersionReader("api-version");
                options.ReportApiVersions = true;
            });

            services.AddVersionedApiExplorer(
                options =>
                {
                    // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                    // note: the specified format code will format the version as "'v'major"
                    options.GroupNameFormat = "'v'V";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;
                });

            services.AddScoped<IUserContext, UserContext>();

            services.AddDistributedMemoryCache();
            services.AddSwagger();

            services.AddOptions<OAuth2Options>()
                    .Bind(Configuration.GetSection(OAuth2Options.SectionName));

            services.AddOptions<OAuth2ClientOptions>()
                    .Bind(Configuration.GetSection(OAuth2ClientOptions.SectionName));

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                var oAuth2Options = new OAuth2Options();
                Configuration.Bind(OAuth2Options.SectionName, oAuth2Options);

                options.Authority = $"https://{oAuth2Options.Domain}";
                options.Audience = oAuth2Options.Audience;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = ClaimTypes.NameIdentifier,
                    RoleClaimType = oAuth2Options.RoleClaimType
                };

                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        // Grab the raw value of the token, and store it as a claim so we can retrieve it again later in the request pipeline
                        if (context.SecurityToken is JwtSecurityToken token)
                            if (context.Principal.Identity is ClaimsIdentity identity)
                            {
                                identity.AddClaim(new Claim("access_token", token.RawData));
                            }

                        return Task.FromResult(0);
                    }
                };
            });

            services
                .AddAuthorization(opts =>
                {
                    opts.AddPolicy(Constants.Authorization.AdminPolicy, policy =>
                    {
                        if (!currentEnvironment.IsDevelopment())
                            policy.RequireRole(Constants.Authorization.AdministratorRoleName);
                        else
                            policy.RequireAuthenticatedUser();
                    });
                });

            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            services.AddCors(options =>
            {
                options.AddPolicy("CORS Policy", corsBuilder.Build());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseCors("CORS Policy");
            if (currentEnvironment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                IdentityModelEventSource.ShowPII = true;
            }

            app.MigrateDatabase();
            if (currentEnvironment.IsDevelopment())
            {
                app.SeedDatabase();
            }

            app.UseMiddleware<GlobalExceptionHandlerMiddleware>();

            app.UseRouting();

            app.UseStaticFiles();

            app.UseAuthentication();
            
            app.UseMiddleware<CurrentUserExistingMiddleware>();
            app.UseSwaggerCustom();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                     "default", "api/{controller=Home}/{action=Index}");
            });
        }
    }
}
