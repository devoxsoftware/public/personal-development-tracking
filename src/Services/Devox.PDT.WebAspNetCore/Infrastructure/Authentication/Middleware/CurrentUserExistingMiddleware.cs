﻿using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Devox.PDT.Application.User.Commands;
using Devox.PDT.Application.User.Models;
using Devox.PDT.Application.User.Queries;
using Devox.PDT.Infrastructure.Authentication.OAuth0;
using Devox.PDT.Infrastructure.Caching.Extensions;
using Devox.PDT.Infrastructure.CurrentUser;
using Devox.PDT.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Devox.PDT.Infrastructure.Authentication.Middleware
{
    public class CurrentUserExistingMiddleware
    {
        private readonly RequestDelegate next;

        public CurrentUserExistingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext, IUserContext userContext,
            IOptions<OAuth2Options> oAuth2Options, MediatR.IMediator mediator,
            IDistributedCache cache)
        {
            if (!httpContext.User.Identity.IsAuthenticated)
            {
                await next.Invoke(httpContext);
                return;
            }

            var claims = httpContext.User.Claims.ToList();
            var userIdentifier = claims.FirstOrDefault(
                it => it.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");

            if (userIdentifier == null)
            {
                throw new UnauthorizedException("Claim cannot be found.");
            }

            var queryResult = await cache.GetAsync<UserModel>(userIdentifier.Value);
            if (queryResult != null)
            {
                LoadFromUserModel(queryResult);
                await next.Invoke(httpContext);
                return;
            }

            queryResult = await mediator.Send(new GetUserByOAuth2Id { OAuth2Id = userIdentifier.Value });
            if (queryResult != null)
            {
                await cache.SetAsync(userIdentifier.Value, queryResult, new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = System.TimeSpan.FromSeconds(oAuth2Options.Value.CacheTokenSec)
                });

                LoadFromUserModel(queryResult);
                await next.Invoke(httpContext);
                return;
            }

            var token = claims.FirstOrDefault(c => c.Type == "access_token").Value;
            if (token == null)
            {
                throw new UnauthorizedException("Access token cannot be found.");
            }
            UserInfo userInfo;
            using (var apiClient = new AuthenticationApiClient(oAuth2Options.Value.Domain))
            {
                userInfo = await apiClient.GetUserInfoAsync(token);
            }

            var userId = await mediator.Send(new CreateUser
            {
                Email = userInfo.Email,
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName,
                PictureUrl = userInfo.Picture,
                OAuth2Id = userIdentifier.Value,
                Metadata = userInfo.GetType()
                            .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                            .ToDictionary(prop => prop.Name, prop => prop.GetValue(userInfo, null)?.ToString())
            });

            queryResult = new UserModel
            {
                Id = userId,
                Email = userInfo.Email,
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName,
                PictureUrl = userInfo.Picture,
                OAuth2Id = userIdentifier.Value,
                Metadata = userInfo.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .ToDictionary(prop => prop.Name, prop => prop.GetValue(userInfo, null)?.ToString())
            };

            await cache.SetAsync(userIdentifier.Value, queryResult, new DistributedCacheEntryOptions
            {
                SlidingExpiration = System.TimeSpan.FromSeconds(oAuth2Options.Value.CacheTokenSec)
            });
            LoadFromUserModel(queryResult);
            await next.Invoke(httpContext);


            void LoadFromUserModel(UserModel model)
            {
                userContext.Id = model.Id;
                userContext.FirstName = model.FirstName;
                userContext.LastName = model.LastName;
                userContext.Email = model.Email;
                userContext.PictureUrl = model.PictureUrl;
                userContext.OAuth2Id = model.OAuth2Id;
                userContext.Metadata = model.Metadata;
            }
        }
    }
}
