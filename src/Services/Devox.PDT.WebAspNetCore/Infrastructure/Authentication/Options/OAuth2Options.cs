﻿namespace Devox.PDT.Infrastructure.Authentication.OAuth0
{
    public class OAuth2Options
    {
        public static string SectionName => "OAuth2";
        public string Domain { get; set; }
        public string Audience { get; set; }
        public int CacheTokenSec { get; set; }
        public string RoleClaimType { get; set; }
    }

}
