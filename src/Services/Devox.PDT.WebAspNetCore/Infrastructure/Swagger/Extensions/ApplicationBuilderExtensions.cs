﻿using Devox.PDT.Infrastructure.Swagger.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;

namespace Devox.PDT.Infrastructure.Swagger.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSwaggerCustom(this IApplicationBuilder applicationBuilder, string pathBase = default)
        {
            if (applicationBuilder == null)
            {
                throw new ArgumentNullException($"{nameof(IApplicationBuilder)} is null.");
            }
            applicationBuilder.UseSwagger();
            var apiVersionDescriptionProvider = applicationBuilder.ApplicationServices.GetRequiredService<IApiVersionDescriptionProvider>();
            var oauth2Options = applicationBuilder.ApplicationServices.GetService<IOptions<OAuth2ClientOptions>>()?.Value;
            applicationBuilder.UseSwaggerUI(options =>
            {
                if (oauth2Options != null)
                {
                    options.OAuthClientId(oauth2Options.ClientId);
                    options.OAuthClientSecret(oauth2Options.ClientSecret);
                    options.OAuthAdditionalQueryStringParams(new Dictionary<string, string> { { "audience", oauth2Options.Audience } });
                }

                foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint(
                        $"{(!string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty)}/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant());
                }

               
            });

            return applicationBuilder;
        }
    }
}
