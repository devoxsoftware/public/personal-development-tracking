﻿using Devox.PDT.Infrastructure.Swagger.Options;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;

namespace Devox.PDT.Infrastructure.Swagger.Extensions
{
    public static class ServiceBuilderExtensions
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var serviceProvider = services.BuildServiceProvider();
                var provider = serviceProvider.GetRequiredService<IApiVersionDescriptionProvider>();

                foreach (var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName, new OpenApiInfo
                    {
                        Title = $"HTTP API {description.ApiVersion}",
                        Version = description.ApiVersion.ToString(),
                        Description = description.IsDeprecated ? "This API version has been deprecated." : string.Empty,
                    });
                }

                var oauth2Config = serviceProvider.GetService<IOptions<OAuth2ClientOptions>>()?.Value;
                if (oauth2Config != null)
                {
                    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                    {
                        Type = SecuritySchemeType.OAuth2,
                        Flows = new OpenApiOAuthFlows
                        {
                            Implicit = new OpenApiOAuthFlow
                            {
                                TokenUrl = new Uri(oauth2Config.TokenUrl),
                                AuthorizationUrl = new Uri(oauth2Config.AuthorizationUrl),
                                Scopes = oauth2Config.Scopes
                            }
                        }
                    });
                }

                options.OperationFilter<SwaggerDefaultValues>();

                options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    { new OpenApiSecurityScheme {
                        Reference = new OpenApiReference {
                            Id = "oauth2", Type = ReferenceType.SecurityScheme } },
                        new List<string>() } });
            });
            return services;
        }



    }
}
