﻿using System.Collections.Generic;

namespace Devox.PDT.Infrastructure.Swagger.Options
{
    public class OAuth2ClientOptions
    {
        public static string SectionName => "Swagger:Authorization:Implicit";
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Audience { get; set; }
        public string AuthorizationUrl { get; set; }
        public string TokenUrl { get; set; }
        public Dictionary<string, string> Scopes { get; set; }
    }
}
