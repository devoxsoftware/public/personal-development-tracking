﻿using Microsoft.Extensions.Caching.Distributed;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Infrastructure.Caching.Extensions
{
    public static class DistributedCacheExtensions
    {
        public async static Task SetAsync<T>(
            this IDistributedCache distributedCache, string key, T value, DistributedCacheEntryOptions options,
            CancellationToken token = default)
        {
            await distributedCache.SetAsync(
                key, JsonSerializer.SerializeToUtf8Bytes(
                        value, new JsonSerializerOptions { IgnoreNullValues = true }), options, token);
        }

        public async static Task<T> GetAsync<T>(
            this IDistributedCache distributedCache, string key, CancellationToken token = default) where T : class
        {
            var result = await distributedCache.GetAsync(key, token);
            return result == null ? null : JsonSerializer.Deserialize<T>(result);
        }
    }
}
