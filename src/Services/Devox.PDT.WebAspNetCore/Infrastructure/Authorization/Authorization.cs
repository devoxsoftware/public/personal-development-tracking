﻿namespace Devox.PDT.Infrastructure.Authorization
{

    public static partial class Constants
    {
        public static class Authorization
        {
            public const string AdminPolicy = "Admin";
            public const string AdministratorRoleName = "Administrator";
        }
    }
}
