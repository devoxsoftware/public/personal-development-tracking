FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /source
COPY ["src/Services/Devox.PDT.WebAspNetCore/Devox.PDT.WebAspNetCore.csproj", "src/Devox.PDT.WebAspNetCore/"]
COPY ["src/Services/Devox.PDT.Application/Devox.PDT.Application.csproj", "src/Devox.PDT.Application/"]
COPY ["src/Services/Devox.PDT.Persistance/Devox.PDT.Persistance.csproj", "src/Devox.PDT.Persistance/"]
COPY ["src/Services/Devox.PDT.Domain/Devox.PDT.Domain.csproj", "src/Devox.PDT.Domain/"]
COPY ["src/Services/Devox.PDT.Infrastructure/Devox.PDT.Infrastructure.csproj", "src/Devox.PDT.Infrastructure/"]

RUN dotnet restore "src/Devox.PDT.WebAspNetCore/Devox.PDT.WebAspNetCore.csproj"
COPY src/Services/ src/
WORKDIR "src/Devox.PDT.WebAspNetCore/"
RUN dotnet build "Devox.PDT.WebAspNetCore.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Devox.PDT.WebAspNetCore.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Devox.PDT.WebAspNetCore.dll"]