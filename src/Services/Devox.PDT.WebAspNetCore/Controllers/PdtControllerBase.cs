﻿using Devox.PDT.Infrastructure.ExceptionHandling;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Devox.PDT.Controllers.V1
{
    [ApiController]
    [Route("api/[controller]")]
    [ProducesResponseType(typeof(ExceptionResult), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(ExceptionResult), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ExceptionResult), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ExceptionResult), StatusCodes.Status500InternalServerError)]
    public abstract class PdtControllerBase : ControllerBase
    {
        protected MediatR.IMediator Mediator { get; }

        protected PdtControllerBase(MediatR.IMediator mediator) => Mediator = mediator;
    }
}
