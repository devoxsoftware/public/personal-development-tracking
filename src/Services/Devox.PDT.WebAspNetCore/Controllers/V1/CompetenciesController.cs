﻿using Devox.PDT.Application.Competency.Commands;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Application.Competency.Queries;
using Devox.PDT.Infrastructure.Authorization;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CompetenciesController : PdtControllerBase
    {
        public CompetenciesController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CompetencyModel>), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetAllCompetencies()
        {
            var competencyListView = await Mediator.Send(new GetAllCompetencies());
            return Ok(competencyListView.Competencies.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(CompetencyModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> GetCompetencyById([FromRoute]int id)
        {
            var competencyModel = await Mediator.Send(new GetCompetencyById { Id = id });
            return Ok(competencyModel);
        }

        [HttpGet]
        [ProducesResponseType(typeof(CompetencyModel), StatusCodes.Status200OK)]
        [Route("find")]
        public async Task<ActionResult> GetCompetencyBySpecializationAndSeniority(
            [FromQuery, Required]int specializationId, [FromQuery, Required]int seniorityId)
        {
            var competencyModel = await Mediator.Send(
                new GetCompetencyBySpecializationAndSeniority { 
                    SpecializationId = specializationId, 
                    SeniorityId = seniorityId
                });
            return Ok(competencyModel);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CompetencyModel), StatusCodes.Status200OK)]
        [Authorize(Policy = Constants.Authorization.AdminPolicy)]
        public async Task<ActionResult> CreateCompetency([FromBody]CreateCompetency competencyBody)
        {
            var competencyModel = await Mediator.Send(competencyBody);
            return Ok(competencyModel);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(CompetencyModel), StatusCodes.Status200OK)]
        [Authorize(Policy = Constants.Authorization.AdminPolicy)]
        public async Task<ActionResult> UpdateCompetency([FromBody]UpdateCompetency competencyBody)
        {
            var competencyModel = await Mediator.Send(competencyBody);
            return Ok(competencyModel);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [Authorize(Policy = Constants.Authorization.AdminPolicy)]
        [Route("{id}")]
        public async Task<ActionResult> DeleteCompetency([FromRoute]int id)
        {
            var competencyId = await Mediator.Send(new DeleteCompetency { Id = id });
            return Ok(competencyId);
        }
    }
}
