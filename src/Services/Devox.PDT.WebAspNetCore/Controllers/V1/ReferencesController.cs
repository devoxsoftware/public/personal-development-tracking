﻿using Devox.PDT.Application.Reference.Commands;
using Devox.PDT.Application.Reference.Models;
using Devox.PDT.Application.Reference.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ReferencesController : PdtControllerBase
    {
        public ReferencesController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ReferenceModel>), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetAllReferences()
        {
            var items = await Mediator.Send(new GetAllReferences { });
            return Ok(items.References.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(ReferenceModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> GetReferenceById([FromRoute]int id)
        {
            var referenceModel = await Mediator.Send(new GetReferenceById { Id = id });
            return Ok(referenceModel);
        }        

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ReferenceModel>), StatusCodes.Status200OK)]
        [Route("~/api/Topics/{id}/[controller]", Order = 0)]
        public async Task<ActionResult> GetReferenceByTopicId([FromRoute]int id)
        {
            var topicListView = await Mediator.Send(new GetReferencesByTopicId { TopicId = id });
            return Ok(topicListView.References.ToList());
        }

        [HttpPost]
        [ProducesResponseType(typeof(ReferenceModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> CreateReference([FromBody]CreateReference referenceBody)
        {
            var referenceModel = await Mediator.Send(referenceBody);
            return Ok(referenceModel);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(ReferenceModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> UpdateReference([FromBody]UpdateReference referenceBody)
        {
            var referenceModel = await Mediator.Send(referenceBody);
            return Ok(referenceModel);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> DeleteReference([FromRoute]int id)
        {
            var referenceId = await Mediator.Send(new DeleteReference { Id = id });
            return Ok(referenceId);
        }
    }
}
