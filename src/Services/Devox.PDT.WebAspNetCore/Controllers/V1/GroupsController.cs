﻿using Devox.PDT.Application.Group.Commands;
using Devox.PDT.Application.Group.Models;
using Devox.PDT.Application.Group.Queries;
using Devox.PDT.Controllers.V1;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.WebAspNetCore.Controllers.Api1dot0
{
    [ApiVersion("1.0")]
    [Authorize]
    public class GroupsController : PdtControllerBase
    {
        public GroupsController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<GroupModel>), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetGroups()
        {
            var groupsListView = await Mediator.Send(new GetGroups { });
            return Ok(groupsListView.Groups.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(GroupModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> GetGroupById([FromRoute]int id)
        {
            var groupModel = await Mediator.Send(new GetGroupById { Id = id });
            return Ok(groupModel);
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<GroupModel>), StatusCodes.Status200OK)]
        [Route("~/api/Competencies/{id}/[controller]", Order = 0)]
        public async Task<ActionResult> GetGroupsByCompetencyId([FromRoute]int id)
        {
            var groupsListView = await Mediator.Send(new GetGroupsByCompetencyId { CompetencyId = id });
            return Ok(groupsListView.Groups.ToList());
        }

        [HttpPost]
        [ProducesResponseType(typeof(GroupModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> CreateGroup([FromBody]CreateGroup groupBody)
        {
            var groupModel = await Mediator.Send(groupBody);
            return Ok(groupModel);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(GroupModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> UpdateGroup([FromBody]UpdateGroup groupBody)
        {
            var groupModel = await Mediator.Send(groupBody);
            return Ok(groupModel);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(GroupModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> DeleteGroup([FromRoute]int id)
        {
            var groupModel = await Mediator.Send(new DeleteGroup { Id = id }).ConfigureAwait(false);
            return Ok(groupModel);
        }
    }
}

