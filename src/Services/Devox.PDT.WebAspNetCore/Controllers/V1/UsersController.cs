﻿using Devox.PDT.Application.User.Models;
using Devox.PDT.Application.User.Queries;
using Devox.PDT.Infrastructure.CurrentUser;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class UsersController : PdtControllerBase
    {
        private readonly IUserContext userContext;

        public UsersController(IMediator mediator, IUserContext userContext) : base(mediator)
        {
            this.userContext = userContext;
        }

        [HttpGet]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        [Route("current")]
        public async Task<ActionResult> GetCurrentUser()
        {
            var userModel = await Mediator.Send(new GetUserById { Id = userContext.Id });
            return Ok(userModel);
        }
    }
}
