﻿using Devox.PDT.Application.CompetencyTopic.Commands;
using Devox.PDT.Application.CompetencyTopic.Models;
using Devox.PDT.Application.CompetencyTopic.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CompetencyTopicController : PdtControllerBase
    {
        public CompetencyTopicController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CompetencyTopicModel>), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetAllCompetenciesTopics()
        {
            var competencyTopicsListView = await Mediator.Send(new GetAllCompetenciesTopics { });
            return Ok(competencyTopicsListView.CompetenciesTopics.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(CompetencyTopicModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> GetCompetencyTopicById([FromRoute]int id)
        {
            var competencyTopicModel = await Mediator.Send(new GetCompetencyTopicById { Id = id });
            return Ok(competencyTopicModel);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CompetencyTopicModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> CreateCompetencyTopic([FromBody]CreateCompetencyTopic compTopicBody)
        {
            var competencyTopicModel = await Mediator.Send(compTopicBody);
            return Ok(competencyTopicModel);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(CompetencyTopicModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> UpdateCompetencyTopic([FromBody]UpdateCompetencyTopic compTopicBody)
        {
            var competencyTopicModel = await Mediator.Send(compTopicBody);
            return Ok(competencyTopicModel);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> DeleteCompetencyTopic([FromRoute] int id)
        {
            var topicId = await Mediator.Send(new DeleteCompetencyTopic { Id = id });
            return Ok(topicId);
        }
    }
}
