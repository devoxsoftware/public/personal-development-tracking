﻿using Devox.PDT.Application.Specialization.Commands;
using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Application.Specialization.Queries;
using Devox.PDT.Controllers.V1;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.WebAspNetCore.Controllers.Api1dot0
{
    [ApiVersion("1.0")]
    [Authorize]
    public class SpecializationsController : PdtControllerBase
    {
        public SpecializationsController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<SpecializationModel>), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetSpecializations()
        {
            var specializationsListView = await Mediator.Send(new GetSpecializations { });
            return Ok(specializationsListView.Specializations.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(SpecializationModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> GetSpecializationById([FromRoute]int id)
        {
            var specializationModel = await Mediator.Send(new GetSpecializationById { Id = id});
            return Ok(specializationModel);
        }

        [HttpPost]
        [ProducesResponseType(typeof(SpecializationModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> CreateSpecialization([FromBody]CreateSpecialization specializationBody)
        {
            var specializationModel = await Mediator.Send(specializationBody);
            return Ok(specializationModel);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(SpecializationModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> UpdateSpecialization([FromBody]UpdateSpecialization specializationBody)
        {
            var specializationModel = await Mediator.Send(specializationBody);
            return Ok(specializationModel);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(SpecializationModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> DeleteSpecialization([FromRoute]int id)
        {
            var specializationId = await Mediator.Send(new DeleteSpecialization { Id = id }).ConfigureAwait(false);
            return Ok(specializationId);
        }
    }
}

