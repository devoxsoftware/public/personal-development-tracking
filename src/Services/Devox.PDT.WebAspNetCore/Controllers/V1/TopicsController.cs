﻿using Devox.PDT.Application.Topic.Commands;
using Devox.PDT.Application.Topic.Models;
using Devox.PDT.Application.Topic.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class TopicsController : PdtControllerBase
    {
        public TopicsController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<TopicModel>), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetAllTopics()
        {
            var topicsListView = await Mediator.Send(new GetAllTopics { });
            return Ok(topicsListView.Topics.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(TopicModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> GetTopicById([FromRoute]int id)
        {
            var topicModel = await Mediator.Send(new GetTopicById { Id = id }).ConfigureAwait(false);
            return Ok(topicModel);
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<TopicModel>), StatusCodes.Status200OK)]
        [Route("~/api/Competencies/{id}/[controller]", Order = 0)]
        public async Task<ActionResult> GetTopicsByCompetencyId([FromRoute]int id)
        {
            var topicsListView = await Mediator.Send(new GetTopicsByCompetencyId { CompetencyId = id });
            return Ok(topicsListView.Topics.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<TopicModel>), StatusCodes.Status200OK)]
        [Route("~/api/Groups/{id}/[controller]", Order = 0)]
        public async Task<ActionResult> GetTopicsByGroupId([FromRoute]int id)
        {
            var topicsListView = await Mediator.Send(new GetTopicsByGroupId { GroupId = id });
            return Ok(topicsListView.Topics.ToList());
        }

        [HttpPost]
        [ProducesResponseType(typeof(TopicModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> CreateTopic([FromBody]CreateTopic topicBody)
        {
            var topicModel = await Mediator.Send(topicBody);
            return Ok(topicModel);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(TopicModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> UpdateTopic([FromBody]UpdateTopic topicBody)
        {
            var topicModel = await Mediator.Send(topicBody).ConfigureAwait(false);
            return Ok(topicModel);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> DeleteTopic(int id)
        {
            var topicId = await Mediator.Send(new DeleteTopic { Id = id });
            return Ok(topicId);
        }
    }
}
