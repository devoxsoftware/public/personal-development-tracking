﻿using Devox.PDT.Application.Seniority.Commands;
using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Application.Seniority.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devox.PDT.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class SenioritiesController : PdtControllerBase
    {
        public SenioritiesController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<SeniorityModel>), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetAllSeniorities()
        {
            var seniorityListView = await Mediator.Send(new GetAllSeniorities { });
            return Ok(seniorityListView.Seniorities.ToList());
        }

        [HttpGet]
        [ProducesResponseType(typeof(SeniorityModel), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> GetSeniorityById([FromRoute]int id)
        {
            var seniorityModel = await Mediator.Send(new GetSeniorityById { Id = id });
            return Ok(seniorityModel);
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<SeniorityModel>), StatusCodes.Status200OK)]
        [Route("~/api/Specializations/{id}/[controller]", Order = 0)]
        public async Task<ActionResult> GetSeniorityBySpecializationId([FromRoute]int id)
        {
            var seniorityListView = await Mediator.Send(new GetSenioritiesBySpecializationId { SpecializationId = id });
            return Ok(seniorityListView.Seniorities.ToList());
        }

        [HttpPost]
        [ProducesResponseType(typeof(SeniorityModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> CreateSeniority([FromBody] CreateSeniority seniorityBody)
        {
            var seniorityModel = await Mediator.Send(seniorityBody);
            return Ok(seniorityModel);
        }

        [HttpPatch]
        [ProducesResponseType(typeof(SeniorityModel), StatusCodes.Status200OK)]
        public async Task<ActionResult> UpdateSeniority([FromBody]UpdateSeniority seniorityBody)
        {
            var seniorityModel = await Mediator.Send(seniorityBody);
            return Ok(seniorityModel);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [Route("{id}")]
        public async Task<ActionResult> DeleteSeniority([FromRoute]int id)
        {
            var seniorityId = await Mediator.Send(new DeleteSeniority { Id = id });
            return Ok(seniorityId);
        }
    }
}
