﻿using AutoMapper;
using Devox.PDT.Application.Seniority.Models;

namespace Devox.PDT.Application.Infrastructure
{
    public class AutomapperConfig : Profile
	{
		public AutomapperConfig()
		{
			ConfigureSpecialization();
            ConfigureUser();
            ConfigureGroup();
            ConfigureCompetency();
            ConfigureReference();
            ConfigureTopic();
            ConfigureSeniority();
            ConfigureCompetencyTopic();
        }
        
        private void ConfigureSpecialization()
        {
            CreateMap<Domain.Entities.Specialization, Specialization.Models.SpecializationModel>()
                .ReverseMap();
        }

        private void ConfigureCompetency()
        {
            CreateMap<Domain.Entities.Competency, Competency.Models.CompetencyModel>()
                .ReverseMap();
        }

        private void ConfigureUser()
        {
            CreateMap<Domain.Entities.User, User.Models.UserModel>()
                .ReverseMap();
        }
        private void ConfigureGroup()
        {
            CreateMap<Domain.Entities.Group, Group.Models.GroupModel>();
        }
        private void ConfigureTopic()
        {
            CreateMap<Domain.Entities.Topic, Topic.Models.TopicModel>();
        }
        private void ConfigureSeniority()
        {
            CreateMap<Domain.Entities.Seniority, SeniorityModel>()
                .ReverseMap();
        }

        private void ConfigureReference()
        {
            CreateMap<Domain.Entities.Reference, Reference.Models.ReferenceModel>()
                .ReverseMap();
        }

        private void ConfigureCompetencyTopic()
        {
            CreateMap<Domain.Entities.CompetencyTopic, CompetencyTopic.Models.CompetencyTopicModel>()
                .ReverseMap();
        }
    }
}
