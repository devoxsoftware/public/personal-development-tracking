﻿using AutoMapper;
using Devox.PDT.Application.Group.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Group.Queries
{
    public class GetGroupsByCompetencyId : IRequest<GroupsListView>
    {
        public int CompetencyId { get; set; }

        public class Validator : AbstractValidator<GetGroupsByCompetencyId>
        {
            public Validator()
            {
                RuleFor(s => s.CompetencyId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetGroupsByCompetencyId, GroupsListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<GroupsListView> Handle(GetGroupsByCompetencyId request, CancellationToken cancellationToken)
            {
                var groups = dbContext.CompetenciesTopics.AsNoTracking()
                    .Include(it => it.Topic)
                    .ThenInclude(it => it.Group)
                    .Where(it => it.CompetencyId == request.CompetencyId)
                    .Select(it => mapper.Map<GroupModel>(it.Topic.Group)).Distinct();


                return new GroupsListView { Groups = groups };
            }
        }
    }
}
