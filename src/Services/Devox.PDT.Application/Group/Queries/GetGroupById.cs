﻿using AutoMapper;
using Devox.PDT.Application.Group.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Group.Queries
{
    public class GetGroupById : IRequest<GroupModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetGroupById>
        {
            public Validator()
            {
                RuleFor(g => g.Id).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<GetGroupById, GroupModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<GroupModel> Handle(GetGroupById request, CancellationToken cancellationToken)
            {
                var group = await dbContext.Groups.AsNoTracking()
                    .SingleOrDefaultAsync(g => g.Id == request.Id);
                if (group == null)
                {
                    return null;
                }

                return mapper.Map<GroupModel>(group);
            }
        }
    }
}
