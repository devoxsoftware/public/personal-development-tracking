﻿using AutoMapper;
using Devox.PDT.Application.Group.Models;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Group.Queries
{
    public class GetGroups : IRequest<GroupsListView>
    {
        public class Handler : IRequestHandler<GetGroups, GroupsListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<GroupsListView> Handle(GetGroups request, CancellationToken cancellationToken)
            {
                return new GroupsListView
                {
                    Groups = await dbContext.Groups.AsNoTracking()
                        .Select(g => mapper.Map<GroupModel>(g))
                        .ToListAsync(cancellationToken)
                };
            }
        }
    }
}
