﻿using System.Collections.Generic;

namespace Devox.PDT.Application.Group.Models
{
    public class GroupsListView
    {
        public IEnumerable<GroupModel> Groups { get; set; }
    }
}
