﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Group.Commands
{
    public class DeleteGroup : IRequest<int>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<DeleteGroup>
        {
            public Validator()
            {
                RuleFor(g => g.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<DeleteGroup, int>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<int> Handle(DeleteGroup request, CancellationToken cancellationToken)
            {
                var group = await dbContext.Groups
                    .SingleOrDefaultAsync(g => g.Id == request.Id);
                if (group == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Group), request.Id);
                }
                dbContext.Groups.Remove(group);
                await dbContext.SaveChangesAsync();

                return request.Id;
            }
        }
    }
}
