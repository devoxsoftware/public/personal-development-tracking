﻿using AutoMapper;
using Devox.PDT.Application.Group.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Group.Commands
{
    public class CreateGroup : IRequest<GroupModel>
    {
        public string Name { get; set; }

        public class Validator : AbstractValidator<CreateGroup>
        {
            public Validator()
            {
                RuleFor(g => g.Name).NotEmpty().NotNull();
            }
        }

        public class Handler : IRequestHandler<CreateGroup, GroupModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<GroupModel> Handle(CreateGroup request, CancellationToken cancellationToken)
            {
                var group = await dbContext.Groups.AsNoTracking()
                    .SingleOrDefaultAsync(g => g.Name == request.Name);
                if (group != null)
                {
                    throw new Infrastructure.Exceptions.ValidationException("Equivalent Group already exists.");
                }
                group = new Domain.Entities.Group
                {
                    Name = request.Name
                };
                await dbContext.Groups.AddAsync(group);
                await dbContext.SaveChangesAsync();

                return mapper.Map<GroupModel>(group);
            }
        }
    }
}
