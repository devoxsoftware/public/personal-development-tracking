﻿using AutoMapper;
using Devox.PDT.Application.Group.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Group.Commands
{
    public class UpdateGroup : IRequest<GroupModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public class Validator : AbstractValidator<UpdateGroup>
        {
            public Validator()
            {
                RuleFor(g => g.Id).NotEmpty();
                RuleFor(g => g.Name).NotEmpty().NotNull();
            }
        }

        public class Handler : IRequestHandler<UpdateGroup, GroupModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<GroupModel> Handle(UpdateGroup request, CancellationToken cancellationToken)
            {
                var group = await dbContext.Groups
                    .SingleOrDefaultAsync(g => g.Id == request.Id);
                if (group == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Group), request.Id);
                }
                group.Name = request.Name;
                await dbContext.SaveChangesAsync();

                return mapper.Map<GroupModel>(group);
            }
        }
    }
}
