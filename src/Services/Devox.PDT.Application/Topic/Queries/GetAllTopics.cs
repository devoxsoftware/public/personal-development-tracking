﻿using AutoMapper;
using Devox.PDT.Application.Topic.Models;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Topic.Queries
{
    public class GetAllTopics : IRequest<TopicsListView>
    {
        public class Handler : IRequestHandler<GetAllTopics, TopicsListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<TopicsListView> Handle(GetAllTopics request,
                CancellationToken cancellationToken)
            {
                var res = await dbContext.Topics.AsNoTracking()
                        .Select(it => mapper.Map<TopicModel>(it)
                        ).ToListAsync();
                return new TopicsListView { Topics = res };
            }
        }
    }
}
