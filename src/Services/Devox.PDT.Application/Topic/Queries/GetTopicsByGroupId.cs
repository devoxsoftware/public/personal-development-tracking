﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Topic.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Topic.Queries
{
    public class GetTopicsByGroupId : IRequest<TopicsListView>
    {
        public int GroupId { get; set; }

        public class Validator : AbstractValidator<GetTopicsByGroupId>
        {
            public Validator()
            {
                RuleFor(s => s.GroupId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetTopicsByGroupId, TopicsListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<TopicsListView> Handle(GetTopicsByGroupId request, CancellationToken cancellationToken)
            {
                 var topics = await dbContext.Topics.AsNoTracking()
                    .Where(it => it.GroupId==request.GroupId)
                    .ToListAsync();

                if (topics.Count() == 0)
                {
                    return null;
                }

                var res = topics.Select(t => mapper.Map<TopicModel>(t)).ToList();
                return new TopicsListView { Topics = res };
            }
        }
    }
}
