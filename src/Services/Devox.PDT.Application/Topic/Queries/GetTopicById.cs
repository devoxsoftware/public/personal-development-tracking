﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Topic.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Topic.Queries
{
    public class GetTopicById : IRequest<TopicModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetTopicById>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetTopicById, TopicModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<TopicModel> Handle(GetTopicById request, CancellationToken cancellationToken)
            {
                var topic = await dbContext.Topics.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.Id == request.Id);

                if (topic == null)
                {
                    return null;
                }
                return mapper.Map<TopicModel>(topic);
            }
        }
    }
}
