﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Topic.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Topic.Queries
{
    public class GetTopicsByCompetencyId : IRequest<TopicsListView>
    {
        public int CompetencyId { get; set; }

        public class Validator : AbstractValidator<GetTopicsByCompetencyId>
        {
            public Validator()
            {
                RuleFor(s => s.CompetencyId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetTopicsByCompetencyId, TopicsListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<TopicsListView> Handle(GetTopicsByCompetencyId request, CancellationToken cancellationToken)
            {
                var topics = await dbContext.Topics.AsNoTracking()
                    .Where(it => it.CompetencyTopic.Any(ct => ct.CompetencyId == request.CompetencyId))
                    .ToListAsync();

                if (topics.Count() == 0)
                {
                    return null;
                }
                var res = topics.Select(t => mapper.Map<TopicModel>(t)).ToList();
                return new TopicsListView { Topics = res };
            }
        }
    }
}
