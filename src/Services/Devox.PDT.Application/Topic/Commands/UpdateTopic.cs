﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Topic.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;


namespace Devox.PDT.Application.Topic.Commands
{
    public class UpdateTopic : IRequest<TopicModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }

        public class Handler : IRequestHandler<UpdateTopic, TopicModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public class Validator : AbstractValidator<UpdateTopic>
            {
                public Validator()
                {
                    RuleFor(s => s.Id).NotEmpty();
                    RuleFor(s => s.Name).NotEmpty().NotNull();
                    RuleFor(s => s.GroupId).NotEmpty();
                }
            }

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<TopicModel> Handle(UpdateTopic request, CancellationToken cancellationToken)
            {
                var topic = await dbContext.Topics
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (topic == null)
                    throw new NotFoundException(nameof(Domain.Entities.Topic), request.Id);

                topic.Name = request.Name;
                topic.GroupId = request.GroupId;

                await dbContext.SaveChangesAsync();
                return mapper.Map<TopicModel>(topic);
            }
        }

    }
}
