﻿using AutoMapper;
using Devox.PDT.Application.Topic.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Topic.Commands
{

    public class CreateTopic : IRequest<TopicModel>
    {
        public string Name { get; set; }
        public int GroupId { get; set; }

        public class Validator : AbstractValidator<CreateTopic>
        {
            public Validator()
            {
                RuleFor(s => s.Name).NotEmpty().NotNull();
                RuleFor(s => s.GroupId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<CreateTopic, TopicModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<TopicModel> Handle(CreateTopic request, CancellationToken cancellationToken)
            {
                var topic = new Domain.Entities.Topic
                {
                    Name = request.Name,
                    GroupId = request.GroupId
                };
                await dbContext.Topics.AddAsync(topic);
                await dbContext.SaveChangesAsync();
                return mapper.Map<TopicModel>(topic);
            }
        }

    }


}