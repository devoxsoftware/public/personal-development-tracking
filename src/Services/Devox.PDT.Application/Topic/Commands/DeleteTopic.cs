﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Topic.Commands
{

    public class DeleteTopic : IRequest<int>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<DeleteTopic>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<DeleteTopic, int>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<int> Handle(DeleteTopic request, CancellationToken cancellationToken)
            {
                var topic = await dbContext.Topics
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (topic == null)
                    throw new NotFoundException(nameof(Domain.Entities.Topic), request.Id);

                dbContext.Topics.Remove(topic);
                await dbContext.SaveChangesAsync();
                return request.Id;
            }
        }
    }
}