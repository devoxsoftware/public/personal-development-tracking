﻿namespace Devox.PDT.Application.Topic.Models
{
    public class TopicModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }
    }
}
