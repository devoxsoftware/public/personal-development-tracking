﻿using System.Collections.Generic;

namespace Devox.PDT.Application.Topic.Models
{
    public class TopicsListView
    {
        public IEnumerable<TopicModel> Topics { get; set; }
    }
}
