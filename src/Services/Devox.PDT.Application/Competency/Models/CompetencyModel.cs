﻿namespace Devox.PDT.Application.Competency.Models
{
    public class CompetencyModel
    {
        public int Id { get; set; }
        public int SpecializationId { get; set; }
        public int SeniorityId { get; set; }
    }
}
