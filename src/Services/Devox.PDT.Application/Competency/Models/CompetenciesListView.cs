﻿using System.Collections.Generic;

namespace Devox.PDT.Application.Competency.Models
{
    public class CompetenciesListView
    {
        public IEnumerable<CompetencyModel> Competencies { get; set; }
    }
}
