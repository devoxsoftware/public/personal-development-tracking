﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;


namespace Devox.PDT.Application.Competency.Commands
{
    public class UpdateCompetency : IRequest<CompetencyModel>
    {
        public int Id { get; set; }
        public int SpecializationId { get; set; }
        public int SeniorityId { get; set; }

        public class Handler : IRequestHandler<UpdateCompetency, CompetencyModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;
            
            public class Validator : AbstractValidator<UpdateCompetency>
            {
                public Validator()
                {
                    RuleFor(s => s.Id).NotEmpty();
                    RuleFor(s => s.SpecializationId).NotEmpty();
                    RuleFor(s => s.SeniorityId).NotEmpty();
                }
            }

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyModel> Handle(UpdateCompetency request, CancellationToken cancellationToken)
            {
                var competency = await dbContext.Competencies
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (competency == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Competency), request.Id);
                }
                competency.SpecializationId = request.SpecializationId;
                competency.SeniorityId = request.SeniorityId;

                await dbContext.SaveChangesAsync();
                return mapper.Map<CompetencyModel>(competency);
            }
        }

    }
}
