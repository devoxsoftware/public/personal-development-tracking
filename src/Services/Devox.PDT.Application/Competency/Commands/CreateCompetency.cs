﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Competency.Commands
{

    public class CreateCompetency : IRequest<CompetencyModel>
    {
        public int SpecializationId { get; set; }
        public int SeniorityId { get; set; }

        public class Validator : AbstractValidator<CreateCompetency>
        {
            public Validator()
            {
                RuleFor(s => s.SpecializationId).NotEmpty();
                RuleFor(s => s.SeniorityId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<CreateCompetency, CompetencyModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyModel> Handle(CreateCompetency request, CancellationToken cancellationToken)
            {
                var competency = await dbContext.Competencies.AsNoTracking()
                    .SingleOrDefaultAsync(it => it.SeniorityId == request.SeniorityId
                                             && it.SpecializationId == request.SpecializationId);
                if (competency != null)
                {
                    throw new Infrastructure.Exceptions.ValidationException("Equivalent Competency already exists.");
                }
                competency = new Domain.Entities.Competency
                {
                    SeniorityId = request.SeniorityId,
                    SpecializationId = request.SpecializationId
                };
                await dbContext.Competencies.AddAsync(competency);
                await dbContext.SaveChangesAsync();
                return mapper.Map<CompetencyModel>(competency);
            }
        }

    }


}