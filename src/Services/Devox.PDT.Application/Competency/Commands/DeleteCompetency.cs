﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Competency.Commands
{

    public class DeleteCompetency : IRequest<int>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<DeleteCompetency>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<DeleteCompetency, int>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<int> Handle(DeleteCompetency request, CancellationToken cancellationToken)
            {
                var competency = await dbContext.Competencies
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (competency == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Competency), request.Id);
                }
                dbContext.Competencies.Remove(competency);
                await dbContext.SaveChangesAsync();
                return request.Id;
            }
        }
    }
}