﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Competency.Queries
{
    public class GetAllCompetencies : IRequest<CompetenciesListView>
    {
        public class Handler : IRequestHandler<GetAllCompetencies, CompetenciesListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetenciesListView> Handle(GetAllCompetencies request,
                CancellationToken cancellationToken)
            {
                var res = await dbContext.Competencies
                        .AsNoTracking()
                        .Select(it => mapper.Map<CompetencyModel>(it)
                        ).ToListAsync();
                return new CompetenciesListView { Competencies = res };
            }
        }
    }
}
