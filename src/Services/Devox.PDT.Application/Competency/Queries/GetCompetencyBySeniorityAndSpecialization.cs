﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Competency.Queries
{
    public class GetCompetencyBySpecializationAndSeniority : IRequest<CompetencyModel>
    {
        public int SpecializationId { get; set; }
        public int SeniorityId { get; set; }

        public class Validator : AbstractValidator<GetCompetencyBySpecializationAndSeniority>
        {
            public Validator()
            {
                RuleFor(s => s.SpecializationId).NotEmpty();
                RuleFor(s => s.SeniorityId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetCompetencyBySpecializationAndSeniority, CompetencyModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyModel> Handle(GetCompetencyBySpecializationAndSeniority request, CancellationToken cancellationToken)
            {
                var competency = await dbContext.Competencies.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.SeniorityId == request.SeniorityId 
                    && it.SpecializationId == request.SpecializationId);

                if (competency == null)
                {
                    return null;
                }
                return mapper.Map<CompetencyModel>(competency);
            }
        }
    }
}
