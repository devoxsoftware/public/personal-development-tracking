﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Competency.Queries
{
    public class GetCompetencyById : IRequest<CompetencyModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetCompetencyById>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetCompetencyById, CompetencyModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyModel> Handle(GetCompetencyById request, CancellationToken cancellationToken)
            {
                var competency = await dbContext.Competencies.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.Id == request.Id);

                if (competency == null)
                {
                    return null;
                }
                return mapper.Map<CompetencyModel>(competency);
            }
        }
    }
}
