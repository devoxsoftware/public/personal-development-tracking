﻿using AutoMapper;
using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Specialization.Commands
{
    public class CreateSpecialization : IRequest<SpecializationModel>
    {
        public string Name { get; set; }

        public class Validator : AbstractValidator<CreateSpecialization>
        {
            public Validator()
            {
                RuleFor(s => s.Name).NotEmpty().NotNull();
            }
        }

        public class Handler : IRequestHandler<CreateSpecialization, SpecializationModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SpecializationModel> Handle(CreateSpecialization request, CancellationToken cancellationToken)
            {
                var specialization = await dbContext.Specializations.AsNoTracking()
                    .SingleOrDefaultAsync(it => it.Name == request.Name);
                if (specialization != null)
                {
                    throw new Infrastructure.Exceptions.ValidationException("Equivalent Specialization already exists.");
                }
                specialization = new Domain.Entities.Specialization
                {
                    Name = request.Name
                };
                await dbContext.Specializations.AddAsync(specialization);
                await dbContext.SaveChangesAsync();

                return mapper.Map<SpecializationModel>(specialization);
            }
        }
    }
}
