﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Specialization.Commands
{
    public class UpdateSpecialization : IRequest<SpecializationModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public class Validator : AbstractValidator<UpdateSpecialization>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
                RuleFor(s => s.Name).NotEmpty().NotNull();
            }
        }

        public class Handler : IRequestHandler<UpdateSpecialization, SpecializationModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SpecializationModel> Handle(UpdateSpecialization request, CancellationToken cancellationToken)
            {
                var specialization = await dbContext.Specializations
                    .SingleOrDefaultAsync(s => s.Id == request.Id);
                if (specialization == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Specialization), request.Id); ;
                }
                specialization.Name = request.Name;                
                await dbContext.SaveChangesAsync();

                return mapper.Map<SpecializationModel>(specialization);
            }
        }
    }
}
