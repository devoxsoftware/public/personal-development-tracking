﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Specialization.Commands
{
    public class DeleteSpecialization : IRequest<int>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<DeleteSpecialization>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<DeleteSpecialization, int>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<int> Handle(DeleteSpecialization request, CancellationToken cancellationToken)
            {
                var specialization = await dbContext.Specializations
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (specialization == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Specialization), request.Id);
                }
                dbContext.Specializations.Remove(specialization);
                await dbContext.SaveChangesAsync();

                return request.Id;
            }
        }
    }
}
