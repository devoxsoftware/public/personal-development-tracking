﻿namespace Devox.PDT.Application.Specialization.Models
{
    public class SpecializationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
