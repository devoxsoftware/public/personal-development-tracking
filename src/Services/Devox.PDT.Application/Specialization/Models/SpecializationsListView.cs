﻿using System.Collections.Generic;

namespace Devox.PDT.Application.Specialization.Models
{
    public class SpecializationsListView
    {
        public IEnumerable<SpecializationModel> Specializations { get; set; }
    }
}
