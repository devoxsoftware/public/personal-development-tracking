﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Persistance;

namespace Devox.PDT.Application.Specialization.Queries
{
    public class GetSpecializations : IRequest<SpecializationsListView>
    {
        public class Handler : IRequestHandler<GetSpecializations, SpecializationsListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SpecializationsListView> Handle(GetSpecializations request, CancellationToken cancellationToken)
            {
                return new SpecializationsListView
                {
                    Specializations = await dbContext.Specializations.AsNoTracking()
                        .Select(c => mapper.Map<SpecializationModel>(c))
                        .ToListAsync(cancellationToken)
                };
            }
        }
    }
}
