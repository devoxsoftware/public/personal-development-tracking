﻿using AutoMapper;
using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Specialization.Queries
{
    public class GetSpecializationById : IRequest<SpecializationModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetSpecializationById>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<GetSpecializationById, SpecializationModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SpecializationModel> Handle(GetSpecializationById request, CancellationToken cancellationToken)
            {
                var specialization = await dbContext.Specializations.AsNoTracking()
                    .SingleOrDefaultAsync(s => s.Id == request.Id);
                if (specialization == null)
                {
                    return null;
                }
                return mapper.Map<SpecializationModel>(specialization);
            }
        }
    }
}
