﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Application.CompetencyTopic.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;


namespace Devox.PDT.Application.CompetencyTopic.Commands
{
    public class UpdateCompetencyTopic : IRequest<CompetencyTopicModel>
    {
        public int Id { get; set; }
        public int TopicId { get; set; }
        public int CompetencyId { get; set; }

        public class Handler : IRequestHandler<UpdateCompetencyTopic, CompetencyTopicModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;
            
            public class Validator : AbstractValidator<UpdateCompetencyTopic>
            {
                public Validator()
                {
                    RuleFor(s => s.Id).NotEmpty();
                    RuleFor(s => s.CompetencyId).NotEmpty();
                    RuleFor(s => s.TopicId).NotEmpty();
                }
            }

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyTopicModel> Handle(UpdateCompetencyTopic request, CancellationToken cancellationToken)
            {
                var competencyTopic = await dbContext.CompetenciesTopics
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (competencyTopic == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.CompetencyTopic), request.Id);
                }
                competencyTopic.CompetencyId = request.CompetencyId;
                competencyTopic.TopicId = request.TopicId;

                await dbContext.SaveChangesAsync();
                return mapper.Map<CompetencyTopicModel>(competencyTopic);
            }
        }

    }
}
