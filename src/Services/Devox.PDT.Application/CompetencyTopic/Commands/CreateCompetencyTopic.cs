﻿using AutoMapper;
using Devox.PDT.Application.CompetencyTopic.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.CompetencyTopic.Commands
{

    public class CreateCompetencyTopic : IRequest<CompetencyTopicModel>
    {
        public int TopicId { get; set; }
        public int CompetencyId { get; set; }

        public class Validator : AbstractValidator<CompetencyTopicModel>
        {
            public Validator()
            {
                RuleFor(s => s.CompetencyId).NotEmpty();
                RuleFor(s => s.TopicId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<CreateCompetencyTopic, CompetencyTopicModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyTopicModel> Handle(CreateCompetencyTopic request, CancellationToken cancellationToken)
            {
                var competencyTopic = await dbContext.CompetenciesTopics.AsNoTracking()
                    .SingleOrDefaultAsync(it => it.TopicId == request.TopicId
                                             && it.CompetencyId == request.CompetencyId);
                if (competencyTopic != null)
                {
                    throw new Infrastructure.Exceptions.ValidationException("Equivalent Competency already exists.");
                }
                competencyTopic = new Domain.Entities.CompetencyTopic
                {
                    CompetencyId = request.CompetencyId,
                    TopicId = request.TopicId
                };
                await dbContext.CompetenciesTopics.AddAsync(competencyTopic);
                await dbContext.SaveChangesAsync();
                return mapper.Map<CompetencyTopicModel>(competencyTopic);
            }
        }

    }


}