﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.CompetencyTopic.Commands
{

    public class DeleteCompetencyTopic : IRequest<int>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<DeleteCompetencyTopic>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<DeleteCompetencyTopic, int>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<int> Handle(DeleteCompetencyTopic request, CancellationToken cancellationToken)
            {
                var competencyTopic = await dbContext.CompetenciesTopics
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (competencyTopic == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.CompetencyTopic), request.Id);
                }
                dbContext.CompetenciesTopics.Remove(competencyTopic);
                await dbContext.SaveChangesAsync();
                return request.Id;
            }
        }
    }
}