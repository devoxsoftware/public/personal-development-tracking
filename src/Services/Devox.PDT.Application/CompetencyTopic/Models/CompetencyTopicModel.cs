﻿namespace Devox.PDT.Application.CompetencyTopic.Models
{
    public class CompetencyTopicModel
    {
        public int Id { get; set; }
        public int TopicId { get; set; }
        public int CompetencyId { get; set; }
    }
}
