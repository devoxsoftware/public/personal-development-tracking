﻿using System.Collections.Generic;

namespace Devox.PDT.Application.CompetencyTopic.Models
{
    public class CompetencyTopicsListView
    {
        public IEnumerable<CompetencyTopicModel> CompetenciesTopics { get; set; }
    }
}
