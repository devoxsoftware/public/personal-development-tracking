﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Application.CompetencyTopic.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.CompetencyTopic.Queries
{
    public class GetCompetencyTopicById : IRequest<CompetencyTopicModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetCompetencyTopicById>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetCompetencyTopicById, CompetencyTopicModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyTopicModel> Handle(GetCompetencyTopicById request, CancellationToken cancellationToken)
            {
                var competencyTopic = await dbContext.CompetenciesTopics.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.Id == request.Id);

                if (competencyTopic == null)
                {
                    return null;
                }
                return mapper.Map<CompetencyTopicModel>(competencyTopic);
            }
        }
    }
}
