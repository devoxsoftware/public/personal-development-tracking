﻿using AutoMapper;
using Devox.PDT.Application.Competency.Models;
using Devox.PDT.Application.CompetencyTopic.Models;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.CompetencyTopic.Queries
{
    public class GetAllCompetenciesTopics : IRequest<CompetencyTopicsListView>
    {
        public class Handler : IRequestHandler<GetAllCompetenciesTopics, CompetencyTopicsListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompetencyTopicsListView> Handle(GetAllCompetenciesTopics request,
                CancellationToken cancellationToken)
            {
                var res = await dbContext.CompetenciesTopics
                        .AsNoTracking()
                        .Select(it => mapper.Map<CompetencyTopicModel>(it)
                        ).ToListAsync();
                return new CompetencyTopicsListView { CompetenciesTopics = res };
            }
        }
    }
}
