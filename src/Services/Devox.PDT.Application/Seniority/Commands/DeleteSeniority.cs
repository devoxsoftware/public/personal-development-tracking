﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Seniority.Commands
{

    public class DeleteSeniority : IRequest<int>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<DeleteSeniority>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<DeleteSeniority, int>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<int> Handle(DeleteSeniority request, CancellationToken cancellationToken)
            {
                var seniority = await dbContext.Seniorities
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (seniority == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Seniority), request.Id);
                }
                dbContext.Seniorities.Remove(seniority);
                await dbContext.SaveChangesAsync();
                return request.Id;
            }

            
        }

    }


}