﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;


namespace Devox.PDT.Application.Seniority.Commands
{
    public class UpdateSeniority : IRequest<SeniorityModel>
    {
        public int Id { get; set; }
        public string NewName { get; set; }

        public class Validator : AbstractValidator<UpdateSeniority>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
                RuleFor(s => s.NewName).NotEmpty().NotNull();
            }
        }
        public class Handler : IRequestHandler<UpdateSeniority, SeniorityModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SeniorityModel> Handle(UpdateSeniority request, CancellationToken cancellationToken)
            {
                var seniority = await dbContext.Seniorities
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (seniority == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Seniority), request.Id);
                }
                seniority.Name = request.NewName;
                await dbContext.SaveChangesAsync();
                return mapper.Map<SeniorityModel>(seniority);
            }
        }

    }
}
