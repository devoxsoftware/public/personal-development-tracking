﻿using AutoMapper;
using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Seniority.Commands
{

    public class CreateSeniority : IRequest<SeniorityModel>
    {
        public string Name { get; set; }

        public class Validator : AbstractValidator<CreateSeniority>
        {
            public Validator()
            {
                RuleFor(s => s.Name).NotEmpty().NotNull();
            }
        }

        public class Handler : IRequestHandler<CreateSeniority, SeniorityModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SeniorityModel> Handle(CreateSeniority request, CancellationToken cancellationToken)
            {
                var seniority = await dbContext.Seniorities.AsNoTracking()
                    .SingleOrDefaultAsync(it => string.Equals(it.Name, request.Name));
                if (seniority != null)
                {
                    throw new Infrastructure.Exceptions.ValidationException("Equivalent Seniority already exists.");
                }
                seniority = new Domain.Entities.Seniority
                {
                    Name = request.Name
                };
                await dbContext.Seniorities.AddAsync(seniority);
                await dbContext.SaveChangesAsync();
                return mapper.Map<SeniorityModel>(seniority);
            }
        }

    }


}