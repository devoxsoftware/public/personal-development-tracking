﻿namespace Devox.PDT.Application.Seniority.Models
{
    public class SeniorityModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
