﻿using System.Collections.Generic;

namespace Devox.PDT.Application.Seniority.Models
{
    public class SenioritiesListView
    {
        public IEnumerable<SeniorityModel> Seniorities { get; set; }
    }
}
