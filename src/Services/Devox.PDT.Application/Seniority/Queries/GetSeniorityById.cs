﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Seniority.Queries
{
    public class GetSeniorityById : IRequest<SeniorityModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetSeniorityById>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetSeniorityById, SeniorityModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SeniorityModel> Handle(GetSeniorityById request, CancellationToken cancellationToken)
            {
                var seniority = await dbContext.Seniorities.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.Id == request.Id);

                if (seniority == null)
                {
                    return null;
                }
                return mapper.Map<SeniorityModel>(seniority);
            }
        }
    }
}
