﻿using AutoMapper;
using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Seniority.Queries
{
    public class GetSenioritiesBySpecializationId : IRequest<SenioritiesListView>
    {
        public int SpecializationId { get; set; }

        public class Validator : AbstractValidator<GetSenioritiesBySpecializationId>
        {
            public Validator()
            {
                RuleFor(s => s.SpecializationId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetSenioritiesBySpecializationId, SenioritiesListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SenioritiesListView> Handle(GetSenioritiesBySpecializationId request, CancellationToken cancellationToken)
            {
                var seniorities = await dbContext.Competencies.AsNoTracking()
                    .Where(it => it.SpecializationId == request.SpecializationId)
                    .Select(c => mapper.Map<SeniorityModel>(c.Seniority))
                    .ToListAsync();

                return new SenioritiesListView { Seniorities = seniorities };
            }
        }
    }
}
