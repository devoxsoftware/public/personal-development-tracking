﻿using AutoMapper;
using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Seniority.Queries
{
    public class GetAllSeniorities : IRequest<SenioritiesListView>
    {
        public class Handler : IRequestHandler<GetAllSeniorities, SenioritiesListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<SenioritiesListView> Handle(GetAllSeniorities request,
                CancellationToken cancellationToken)
            {
                var res = await dbContext.Seniorities.AsNoTracking()
                        .Select(it => mapper.Map<SeniorityModel>(it))
                        .ToListAsync();
                return new SenioritiesListView { Seniorities = res };
            }
        }
    }
}
