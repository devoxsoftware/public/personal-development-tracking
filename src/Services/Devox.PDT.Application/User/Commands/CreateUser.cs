﻿using AutoMapper;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.User.Commands
{
    public class CreateUser : IRequest<int>
    {        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PictureUrl { get; set; }
        public string OAuth2Id { get; set; }
        public IDictionary<string,string> Metadata { get; set; }

        public class Handler : IRequestHandler<CreateUser, int>
        {
            private readonly PdtDbContext dbContext;

            public Handler(PdtDbContext dbContext)
            {
                this.dbContext = dbContext;
            }

            public async Task<int> Handle(CreateUser request, CancellationToken cancellationToken)
            {
                var user = await dbContext.Users.AsNoTracking()
                    .SingleOrDefaultAsync(it => string.Equals(it.OAuth2Id, request.OAuth2Id));
                if (user != null)
                {
                    throw new Infrastructure.Exceptions.ValidationException("User with given OAuth2Id already exists");
                }
                user = new Domain.Entities.User 
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                    PictureUrl = request.PictureUrl,
                    OAuth2Id = request.OAuth2Id,
                    Metadata = request.Metadata
                };
                await dbContext.Users.AddAsync(user);
                await dbContext.SaveChangesAsync();
                return user.Id;
            }
        }
    }
}
