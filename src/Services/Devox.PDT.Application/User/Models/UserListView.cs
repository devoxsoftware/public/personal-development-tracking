﻿using System.Collections.Generic;

namespace Devox.PDT.Application.User.Models
{
    public class UserListView
    {
        public IEnumerable<UserModel> Users { get; set; }
    }
}
