﻿using AutoMapper;
using Devox.PDT.Application.User.Models;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.User.Queries
{
    public class GetUsers : IRequest<UserListView>
    {
        public class Handler : IRequestHandler<GetUsers, UserListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<UserListView> Handle(GetUsers request, CancellationToken cancellationToken)
            {
                var res = await dbContext.Users.AsNoTracking()
                        .Select(it => new UserModel
                        {
                            Id = it.Id,                            
                            FirstName = it.FirstName,
                            LastName = it.LastName,
                            Email = it.Email,
                            PictureUrl = it.PictureUrl,
                            OAuth2Id = it.OAuth2Id,
                            Metadata = it.Metadata
                        }).ToListAsync();
                return new UserListView { Users = res };
            }          
        }
    }
}
