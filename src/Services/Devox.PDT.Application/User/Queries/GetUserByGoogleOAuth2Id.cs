﻿using AutoMapper;
using Devox.PDT.Application.User.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.User.Queries
{
    public class GetUserByOAuth2Id : IRequest<UserModel>
    {
        public string OAuth2Id { get; set; }

        public class Validator : AbstractValidator<GetUserByOAuth2Id>
        {
            public Validator()
            {
                RuleFor(s => s.OAuth2Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetUserByOAuth2Id, UserModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<UserModel> Handle(GetUserByOAuth2Id request, CancellationToken cancellationToken)
            {
                var user = await dbContext.Users.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.OAuth2Id == request.OAuth2Id);

                if (user == null)
                {
                    return null;
                }
                return mapper.Map<UserModel>(user);
            }
        }
    }
}
