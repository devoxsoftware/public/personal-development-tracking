﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.User.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.User.Queries
{
    public class GetUserById: IRequest<UserModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetUserById>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetUserById, UserModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<UserModel> Handle(GetUserById request, CancellationToken cancellationToken)
            {
                var user = await dbContext.Users.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.Id == request.Id);

                if (user == null)
                {
                    return null;
                }
                return mapper.Map<UserModel>(user);
            }
        }
    }
}
