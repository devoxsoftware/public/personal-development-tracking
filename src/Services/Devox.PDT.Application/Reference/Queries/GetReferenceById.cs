﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Reference.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Reference.Queries
{
    public class GetReferenceById : IRequest<ReferenceModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetReferenceById>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetReferenceById, ReferenceModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ReferenceModel> Handle(GetReferenceById request, CancellationToken cancellationToken)
            {
                var reference = await dbContext.References.AsNoTracking()
                    .FirstOrDefaultAsync(it => it.Id == request.Id);

                if (reference == null)
                {
                    return null;
                }
                return mapper.Map<ReferenceModel>(reference);
            }
        }
    }
}
