﻿using AutoMapper;
using Devox.PDT.Application.Reference.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Reference.Queries
{
    public class GetReferencesByTopicId : IRequest<ReferencesListView>
    {
        public int TopicId { get; set; }

        public class Validator : AbstractValidator<GetReferencesByTopicId>
        {
            public Validator()
            {
                RuleFor(s => s.TopicId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetReferencesByTopicId, ReferencesListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ReferencesListView> Handle(GetReferencesByTopicId request, CancellationToken cancellationToken)
            {
                var reference = await dbContext.References.AsNoTracking()
                    .Where(r => r.TopicId == request.TopicId)
                    .Select(r => mapper.Map<ReferenceModel>(r))
                    .ToListAsync();

                return new ReferencesListView { References = reference };
            }
        }
    }
}
