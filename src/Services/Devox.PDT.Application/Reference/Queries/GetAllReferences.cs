﻿using AutoMapper;
using Devox.PDT.Application.Reference.Models;
using Devox.PDT.Persistance;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Reference.Queries
{
    public class GetAllReferences : IRequest<ReferencesListView>
    {
        public class Handler : IRequestHandler<GetAllReferences, ReferencesListView>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ReferencesListView> Handle(GetAllReferences request,
                CancellationToken cancellationToken)
            {
                var res = await dbContext.References.AsNoTracking()
                        .Select(it => mapper.Map<ReferenceModel>(it)
                        ).ToListAsync();
                return new ReferencesListView { References = res };
            }
        }
    }
}
