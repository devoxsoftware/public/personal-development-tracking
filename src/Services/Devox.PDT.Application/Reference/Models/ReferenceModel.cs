﻿namespace Devox.PDT.Application.Reference.Models
{
    public class ReferenceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TopicId { get; set; }
        public string Link { get; set; }

    }
}
