﻿using System.Collections.Generic;

namespace Devox.PDT.Application.Reference.Models
{
    public class ReferencesListView
    {
        public IEnumerable<ReferenceModel> References { get; set; }
    }
}
