﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Reference.Commands
{

    public class DeleteReference : IRequest<int>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<DeleteReference>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<DeleteReference, int>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<int> Handle(DeleteReference request, CancellationToken cancellationToken)
            {
                var reference = await dbContext.References
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (reference == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Reference), request.Id);
                }
                dbContext.References.Remove(reference);
                await dbContext.SaveChangesAsync();
                return request.Id;
            }
        }
    }
}