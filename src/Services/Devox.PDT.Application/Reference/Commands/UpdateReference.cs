﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Reference.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;


namespace Devox.PDT.Application.Reference.Commands
{
    public class UpdateReference : IRequest<ReferenceModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TopicId { get; set; }
        public string Link { get; set; }

        public class Validator : AbstractValidator<UpdateReference>
        {
            public Validator()
            {
                RuleFor(s => s.Id).NotEmpty();
                RuleFor(s => s.Name).NotEmpty().NotNull();
                RuleFor(s => s.TopicId).NotEmpty();
                RuleFor(s => s.Link).NotEmpty().NotNull();
            }
        }

        public class Handler : IRequestHandler<UpdateReference, ReferenceModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ReferenceModel> Handle(UpdateReference request, CancellationToken cancellationToken)
            {
                var reference = await dbContext.References
                    .SingleOrDefaultAsync(it => it.Id == request.Id);
                if (reference == null)
                {
                    throw new NotFoundException(nameof(Domain.Entities.Reference), request.Id);
                }
                reference.Name = request.Name;
                reference.TopicId = request.TopicId;
                reference.Link = request.Link;

                await dbContext.SaveChangesAsync();
                return mapper.Map<ReferenceModel>(reference);
            }
        }

    }
}
