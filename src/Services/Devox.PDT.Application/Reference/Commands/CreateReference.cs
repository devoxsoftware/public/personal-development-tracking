﻿using AutoMapper;
using Devox.PDT.Application.Reference.Models;
using Devox.PDT.Persistance;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Application.Reference.Commands
{

    public class CreateReference : IRequest<ReferenceModel>
    {
        public string Name { get; set; }
        public int TopicId { get; set; }
        public string Link { get; set; }

        public class Validator : AbstractValidator<CreateReference>
        {
            public Validator()
            {
                RuleFor(s => s.TopicId).NotEmpty();
                RuleFor(s => s.Name).NotEmpty().NotNull();
            }
        }

        public class Handler : IRequestHandler<CreateReference, ReferenceModel>
        {
            private readonly PdtDbContext dbContext;
            private readonly IMapper mapper;

            public Handler(PdtDbContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ReferenceModel> Handle(CreateReference request, CancellationToken cancellationToken)
            {
                var reference = new Domain.Entities.Reference
                {
                    Link = request.Link,
                    Name = request.Name,
                    TopicId = request.TopicId,
                };
                await dbContext.References.AddAsync(reference);
                await dbContext.SaveChangesAsync();
                return mapper.Map<ReferenceModel>(reference);
            }
        }

    }


}