﻿using System.Collections.Generic;

namespace Devox.PDT.Domain.Entities
{
    public class Topic : EntityBase
    {
        public Topic()
        {
            Reference = new HashSet<Reference>();
            CompetencyTopic = new HashSet<CompetencyTopic>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }
        public virtual ICollection<Reference> Reference { get; set; }
        public virtual ICollection<CompetencyTopic> CompetencyTopic { get; set; }
    }
}
