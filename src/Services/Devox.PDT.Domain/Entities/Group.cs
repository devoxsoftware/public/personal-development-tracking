﻿using System.Collections.Generic;

namespace Devox.PDT.Domain.Entities
{
    public class Group : EntityBase
    {
        public Group()
        {
            Topic = new HashSet<Topic>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Topic> Topic { get; set; }
    }
}
