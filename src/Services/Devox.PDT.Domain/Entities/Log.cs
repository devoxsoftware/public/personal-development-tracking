﻿using System;

namespace Devox.PDT.Domain.Entities
{
    public class Log
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string MessageTemplate { get; set; }
        public string Level { get; set; }
        public DateTime TimeStamp { get; set; }//datetimeoffset(7)
        public string Exception { get; set; }
        public string Properties { get; set; }//xml
        public string LogEvent { get; set; }
    }
}
