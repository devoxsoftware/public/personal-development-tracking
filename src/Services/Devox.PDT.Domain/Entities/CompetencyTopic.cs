﻿namespace Devox.PDT.Domain.Entities
{
    public class CompetencyTopic : EntityBase
    {
        public int Id { get; set; }
        public int CompetencyId { get; set; }
        public int TopicId { get; set; }

        public virtual Competency Competency { get; set; }
        public virtual Topic Topic { get; set; }
    }
}
