﻿using System.Collections.Generic;

namespace Devox.PDT.Domain.Entities
{
    public class Specialization: EntityBase
    {
        public Specialization()
        {
            Competency = new HashSet<Competency>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Competency> Competency { get; set; }
    }
}
