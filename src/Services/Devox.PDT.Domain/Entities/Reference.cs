﻿namespace Devox.PDT.Domain.Entities
{
    public class Reference : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TopicId { get; set; }
        public string Link { get; set; }

        public virtual Topic Topic { get; set; }
    }
}
