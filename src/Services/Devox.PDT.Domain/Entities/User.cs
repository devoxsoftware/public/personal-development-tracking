﻿using System.Collections.Generic;

namespace Devox.PDT.Domain.Entities
{
    public class User
    {
        public User()
        {
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PictureUrl { get; set; }
        public string OAuth2Id { get; set; }
        public IDictionary<string,string> Metadata { get; set; }

    }

}
