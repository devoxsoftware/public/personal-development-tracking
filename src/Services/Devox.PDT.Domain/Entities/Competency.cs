﻿using System.Collections.Generic;

namespace Devox.PDT.Domain.Entities
{
    public class Competency : EntityBase
    {
        public Competency()
        {
            CompetencyTopic = new HashSet<CompetencyTopic>();
        }

        public int Id { get; set; }
        public int SpecializationId { get; set; }
        public int SeniorityId { get; set; }

        public virtual Seniority Seniority { get; set; }
        public virtual Specialization Specialization { get; set; }
        public virtual ICollection<CompetencyTopic> CompetencyTopic { get; set; }
    }
}
