﻿using Devox.PDT.Infrastructure.CurrentUser;
using Devox.PDT.Persistance;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Devox.PDT.Application.Tests
{
    public class TestBase : IDisposable
    {
        protected readonly PdtDbContext context;

        public TestBase() => context = CreateDbContext(); 

        static readonly UserContext DummyUserContext = new UserContext()
        {
            Id = 1,
            OAuth2Id = "system",
            Email = "system@devoxsoftware.com",
            FirstName = "System",
            LastName = "System",
            PictureUrl = "",
            Metadata = new Dictionary<string, string>()
        };

        public PdtDbContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<PdtDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var dbContext = new PdtDbContext(options, DummyUserContext);
            dbContext.Database.EnsureCreated();


            FillInGroups(dbContext);
            FillInSpecializations(dbContext);
            FillInSeniorities(dbContext);
            FillInCompetencies(dbContext);
            FillInTopics(dbContext);
            FillInReferences(dbContext);
            FillInCompetenciesTopics(dbContext);
            FillInUsers(dbContext);

            dbContext.SaveChanges();
            return dbContext;
        }

        public static void Destroy(PdtDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }

        public void Dispose() => Destroy(context);

        private static void FillInGroups(PdtDbContext context)
        {
            context.Groups.AddRange(new[] {
                new Domain.Entities.Group { Id = 10, Name = "group1" },
                new Domain.Entities.Group { Id = 20, Name = "group2" },
                new Domain.Entities.Group { Id = 30, Name = "group3" }
            });

        }

        private static void FillInSpecializations(PdtDbContext context)
        {
            context.Specializations.AddRange(new[] {
                new Domain.Entities.Specialization { Id = 10, Name = "specialization1" },
                new Domain.Entities.Specialization { Id = 20, Name = "specialization2" },
                new Domain.Entities.Specialization { Id = 30, Name = "specialization3" }
            });
        }

        private static void FillInSeniorities(PdtDbContext context)
        {
            context.Seniorities.AddRange(new[] {
                new Domain.Entities.Seniority { Id = 10, Name = "seniority1" },
                new Domain.Entities.Seniority { Id = 20, Name = "seniority2" },
                new Domain.Entities.Seniority { Id = 30, Name = "seniority3" }
            });
        }

        private static void FillInCompetencies(PdtDbContext context)
        {
            context.Competencies.AddRange(new[] {
                new Domain.Entities.Competency { Id = 10, SeniorityId = 10, SpecializationId = 10 },
                new Domain.Entities.Competency { Id = 20, SeniorityId = 20, SpecializationId = 20 },
                new Domain.Entities.Competency { Id = 30, SeniorityId = 30, SpecializationId = 30 }
            });
        }

        private static void FillInTopics(PdtDbContext context)
        {
            context.Topics.AddRange(new[] {
                new Domain.Entities.Topic { Id = 10, Name = "topic1", GroupId = 10 },
                new Domain.Entities.Topic { Id = 20, Name = "topic2", GroupId = 20 },
                new Domain.Entities.Topic { Id = 30, Name = "topic3", GroupId = 30 },
                new Domain.Entities.Topic { Id = 40, Name = "topic3", GroupId = 30 }
            });
        }

        private static void FillInReferences(PdtDbContext context)
        {
            context.References.AddRange(new[] {
                new Domain.Entities.Reference { Id = 10, Name = "reference1", TopicId = 10, Link = "link1" },
                new Domain.Entities.Reference { Id = 20, Name = "reference2", TopicId = 20, Link = "link2" },
                new Domain.Entities.Reference { Id = 30, Name = "reference3", TopicId = 30, Link = "link3" }
            });
        }

        private static void FillInCompetenciesTopics(PdtDbContext context)
        {
            context.CompetenciesTopics.AddRange(new[] {
                new Domain.Entities.CompetencyTopic {  Id = 10, CompetencyId=10, TopicId = 10 },
                new Domain.Entities.CompetencyTopic {  Id = 20, CompetencyId=20, TopicId = 20 },
                new Domain.Entities.CompetencyTopic {  Id = 30, CompetencyId=30, TopicId = 30 },
                new Domain.Entities.CompetencyTopic {  Id = 40, CompetencyId=30, TopicId = 40 }
            });
        }
        private static void FillInUsers(PdtDbContext context)
        {
            context.Users.Add(new Domain.Entities.User
            {
                OAuth2Id = DummyUserContext.OAuth2Id,
                Email = DummyUserContext.Email,
                FirstName = DummyUserContext.FirstName,
                LastName = DummyUserContext.LastName,
                PictureUrl = DummyUserContext.PictureUrl,
                Metadata = DummyUserContext.Metadata
            });
        }
        

    }


}
