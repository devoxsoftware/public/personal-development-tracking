﻿using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Application.Seniority.Queries;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Seniorities
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetSenioritiesTest_Positive()
        {
            var handler = new GetAllSeniorities.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetAllSeniorities { }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Seniorities.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<SenioritiesListView>();
        }

        [Fact]
        public async Task GetSeniorityByIdTest_Exist_Positive()
        {
            var handler = new GetSeniorityById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            entitiesBefore.Count.ShouldBe(3);
            var Seniority = context.Seniorities.FirstOrDefault();
            Seniority.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetSeniorityById { Id = Seniority.Id }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Seniorities.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<SeniorityModel>();
            result.Id.ShouldBe(Seniority.Id);
            result.Name.ShouldBe(Seniority.Name);
        }

        [Fact]
        public async Task GetSeniorityByIdTest_NonExist_Negative()
        {
            var handler = new GetSeniorityById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            entitiesBefore.Count.ShouldBe(3);
            int nonExistentId = -999;

            //Act
            var result = await handler.Handle(new GetSeniorityById { Id = nonExistentId }, CancellationToken.None);

            //Assert
            result.ShouldBeNull();
            var entitiesAfter = context.Seniorities.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
        }

        [Fact]
        public async Task GetSenioritiesBySpecializationIdTest_Exist_Positive()
        {
            var handler = new GetSenioritiesBySpecializationId.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var specialization = context.Specializations.FirstOrDefault();
            specialization.ShouldNotBeNull();
            int specializationId = specialization.Id;
            //Act
            var result = await handler.Handle(
                new GetSenioritiesBySpecializationId { SpecializationId = specializationId }, CancellationToken.None);

            //Assert
            result.ShouldBeOfType<SenioritiesListView>();
            result.ShouldNotBeNull();
            result.Seniorities.Count().ShouldBe(1);
        }

        [Fact]
        public async Task GetSenioritiesBySpecializationIdTest_NonExist_Negative()
        {
            var handler = new GetSenioritiesBySpecializationId.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            int nonExistentId = -999;

            //Act
            var result = await handler.Handle(
                new GetSenioritiesBySpecializationId { SpecializationId = nonExistentId }, CancellationToken.None);


            //Assert
            result.ShouldBeOfType<SenioritiesListView>();
            result.Seniorities.Count().ShouldBe(0);            
        }
    }
}
