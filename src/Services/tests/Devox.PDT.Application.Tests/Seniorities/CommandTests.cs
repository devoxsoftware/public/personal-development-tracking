﻿using Devox.PDT.Application.Seniority.Commands;
using Devox.PDT.Application.Seniority.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Seniorities
{
    public class SeniorityTests : TestBase
    {
        [Fact]
        public async Task CreateSeniorityTest_Positive()
        {
            var handler = new CreateSeniority.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotContain(c => c.Name.Equals("test"));

            //Act
            var result = await handler.Handle(new CreateSeniority { Name = "test" }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Seniorities.ToList();
            entitiesAfter.Count.ShouldBe(4);
            entitiesAfter.ShouldContain(c => c.Name.Equals("test"));
            result.ShouldBeOfType<SeniorityModel>();
        }

        [Fact]
        public async Task CreateSeniorityTest_Negative_The_Same_Name()
        {
            var handler = new CreateSeniority.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var countBefore = context.Seniorities.Count();
            var entityWhichExists = context.Seniorities.First();
            //Act
            Func<Task> result = async () => await handler.Handle(
                new CreateSeniority { Name = entityWhichExists.Name }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(ValidationException));
            var countAfter = context.Seniorities.Count();
            countBefore.ShouldBe(countAfter);
        }

        [Fact]
        public async Task DeleteSeniorityTest_Exist_Pisitive()
        {
            var handler = new DeleteSeniority.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            var Seniority = context.Seniorities.FirstOrDefault();
            entitiesBefore.Count.ShouldBe(3);

            //Act
            var result = await handler.Handle(new DeleteSeniority { Id = Seniority.Id }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Seniorities.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count - 1);
            entitiesAfter.ShouldNotContain(g => g.Id == Seniority.Id);
            result.ShouldBe(Seniority.Id);
        }

        [Fact]
        public async Task DeleteSeniorityTest_NonExist_Negative()
        {
            var handler = new DeleteSeniority.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            int deleteId = 11;
            entitiesBefore.Count.ShouldBe(3);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new DeleteSeniority { Id = deleteId }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
            var entitiesAfter = context.Seniorities.ToList();
            entitiesBefore.Count.ShouldBe(entitiesAfter.Count);
        }

        [Fact]
        public async Task UpdateSeniorityTest_Exist_Positive()
        {
            var handler = new UpdateSeniority.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            var Seniority = context.Seniorities.FirstOrDefault();
            string updateName = "test";
            entitiesBefore.Count.ShouldBe(3);
            Seniority.ShouldNotBeNull();
            //Act
            var result = await handler.Handle(
                new UpdateSeniority { Id = Seniority.Id, NewName = updateName }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Seniorities.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            entitiesAfter.Where(g => g.Id == Seniority.Id).ShouldContain(g => g.Name == updateName);
            result.ShouldBeOfType<SeniorityModel>();
        }

        [Fact]
        public async Task UpdateSeniorityTest_NonExist_Negative()
        {
            var handler = new UpdateSeniority.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Seniorities.ToList();
            int deleteId = 11;
            string updateName = "test";
            entitiesBefore.Count.ShouldBe(3);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new UpdateSeniority { Id = deleteId, NewName = updateName }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
            var entitiesAfter = context.Seniorities.ToList();
            entitiesBefore.Count.ShouldBe(entitiesAfter.Count);
        }
    }
}
