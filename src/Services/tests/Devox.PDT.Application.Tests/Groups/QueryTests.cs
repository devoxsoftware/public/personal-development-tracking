﻿using Devox.PDT.Application.Group.Models;
using Devox.PDT.Application.Group.Queries;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Groups
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetGroupsTest_Positive()
        {
            var handler = new GetGroups.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetGroups { }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Groups.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<GroupsListView>();
        }

        [Fact]
        public async Task GetGroupByIdTest_Exist_Positive()
        {
            var handler = new GetGroupById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            entitiesBefore.Count.ShouldBe(3);
            var group = context.Groups.FirstOrDefault();
            group.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetGroupById { Id = group.Id }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Groups.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<GroupModel>();
            result.Id.ShouldBe(group.Id);
            result.Name.ShouldBe(group.Name);
        }

        [Fact]
        public async Task GetGroupByIdTest_NonExist_Negative()
        {
            var handler = new GetGroupById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            entitiesBefore.Count.ShouldBe(3);
            int nonExistentId = -999;

            //Act
            var result = await handler.Handle(new GetGroupById { Id = nonExistentId }, CancellationToken.None);

            //Assert
            result.ShouldBeNull();
            var entitiesAfter = context.Groups.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
        }

        [Fact]
        public async Task GetGroupsByCompetencyIdTest_Exist_Positive()
        {
            var handler = new GetGroupsByCompetencyId.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var competency = context.Competencies.Where(c=>c.Id==20).FirstOrDefault();
            competency.ShouldNotBeNull();
            int competencyId = competency.Id;

            //Act
            var result = await handler.Handle(
                new GetGroupsByCompetencyId { CompetencyId = competencyId }, CancellationToken.None);

            //Assert
            result.ShouldBeOfType<GroupsListView>();
            result.ShouldNotBeNull();
            result.Groups.Count().ShouldBe(1);
        }

        [Fact]
        public async Task GetGroupsByCompetencyIdTest_NonExist_Negative()
        {
            var handler = new GetGroupsByCompetencyId.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            int nonExistentId = -999;

            //Act
            var result = await handler.Handle(
                new GetGroupsByCompetencyId { CompetencyId = nonExistentId }, CancellationToken.None);

            //Assert
            result.Groups.Count().ShouldBe(0);
            result.ShouldBeOfType<GroupsListView>();
        }
    }
}
