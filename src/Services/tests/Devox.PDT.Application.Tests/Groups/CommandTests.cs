using Devox.PDT.Application.Group.Commands;
using Devox.PDT.Application.Group.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Groups
{
    public class CommandTests : TestBase
    {        
        [Fact]
        public async Task CreateGroupTest_Positive()
        {
            var handler = new CreateGroup.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotContain(c => c.Name.Equals("test"));

            //Act
            var result = await handler.Handle(new CreateGroup { Name = "test" }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Groups.ToList();
            entitiesAfter.Count.ShouldBe(4);
            entitiesAfter.ShouldContain(c => c.Name.Equals("test"));
            result.ShouldBeOfType<GroupModel>();
        }

        [Fact]
        public async Task CreateGroupTest_Negative_The_Same_Name()
        {
            var handler = new CreateGroup.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var countBefore = context.Groups.Count();
            var entityWhichExists = context.Groups.First();
            //Act
            Func<Task> result = async () => await handler.Handle(
                new CreateGroup { Name = entityWhichExists.Name }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(ValidationException));
            var countAfter = context.Groups.Count();
            countBefore.ShouldBe(countAfter);
        }

        [Fact]
        public async Task DeleteGroupTest_Exist_Pisitive()
        {
            var handler = new DeleteGroup.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            var group = context.Groups.FirstOrDefault();
            entitiesBefore.Count.ShouldBe(3);

            //Act
            var result = await handler.Handle(new DeleteGroup { Id = group.Id }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Groups.ToList();            
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count - 1);
            entitiesAfter.ShouldNotContain(g => g.Id == group.Id);
            result.ShouldBe(group.Id);
        }

        [Fact]
        public async Task DeleteGroupTest_NonExist_Negative()
        {
            var handler = new DeleteGroup.Handler(context, AutomapperHelper.Mapper);            

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            int deleteId = 11;
            entitiesBefore.Count.ShouldBe(3);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new DeleteGroup { Id = deleteId }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
            var entitiesAfter = context.Groups.ToList();
            entitiesBefore.Count.ShouldBe(entitiesAfter.Count);
        }

        [Fact]
        public async Task UpdateGroupTest_Exist_Positive()
        {
            var handler = new UpdateGroup.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            var group = context.Groups.FirstOrDefault();
            string updateName = "test";
            entitiesBefore.Count.ShouldBe(3);
            group.ShouldNotBeNull();
            //Act
            var result = await handler.Handle(
                new UpdateGroup { Id = group.Id, Name = updateName }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Groups.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            entitiesAfter.Where(g => g.Id == group.Id).ShouldContain(g=>g.Name == updateName);
            result.ShouldBeOfType<GroupModel>();
        }

        [Fact]
        public async Task UpdateGroupTest_NonExist_Negative()
        {
            var handler = new UpdateGroup.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Groups.ToList();
            int deleteId = 11;
            string updateName = "test";
            entitiesBefore.Count.ShouldBe(3);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new UpdateGroup { Id = deleteId, Name = updateName }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
            var entitiesAfter = context.Groups.ToList();
            entitiesBefore.Count.ShouldBe(entitiesAfter.Count);
        }
    }
}
