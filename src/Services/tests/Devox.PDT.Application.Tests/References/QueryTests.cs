﻿using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.Reference.Queries;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Devox.PDT.Application.Reference.Models;

namespace Devox.PDT.Application.Tests.References
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetAllReferencesTest_Positive()
        {
            var handler = new GetAllReferences.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var referenceCount = context.References.Count();

            //Act
            var result = await handler.Handle(new GetAllReferences {}, CancellationToken.None);

            //Assert
            result.References.Count().ShouldBe(referenceCount);
        }

        [Fact]
        public async Task GetReferenceByIdTest_Positive()
        {
            var handler = new GetReferenceById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var reference = context.References.FirstOrDefault();
            reference.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetReferenceById { Id = reference.Id }, CancellationToken.None);

            //Assert
            result.Id.ShouldBe(reference.Id);
            result.Name.ShouldBe(reference.Name);
            result.TopicId.ShouldBe(reference.TopicId);
        }

        [Fact]
        public async Task GetReferenceById_Negative_ReferenceNotFound()
        {
            var handler = new GetReferenceById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.References.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            var result = await handler.Handle(
                new GetReferenceById { Id = -1}, CancellationToken.None);

            //Assert
            result.ShouldBe(null);
        }

        [Fact]
        public async Task GetReferencesByTopicIdTest_Exist_Positive()
        {
            var handler = new GetReferencesByTopicId.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var topic = context.Topics.FirstOrDefault();
            topic.ShouldNotBeNull();
            int topicId = topic.Id;
            //Act
            var result = await handler.Handle(
                new GetReferencesByTopicId { TopicId = topicId }, CancellationToken.None);

            //Assert
            result.ShouldBeOfType<ReferencesListView>();
            result.ShouldNotBeNull();
            result.References.Count().ShouldBe(1);
        }

        [Fact]
        public async Task GetReferencesByTopicIdTest_NonExist_Negative()
        {
            var handler = new GetReferencesByTopicId.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            int nonExistentId = -999;

            //Act
            var result = await handler.Handle(
                new GetReferencesByTopicId { TopicId = nonExistentId }, CancellationToken.None);

            //Assert
            result.ShouldBeOfType<ReferencesListView>();
            result.References.Count().ShouldBe(0);
        }
    }
}
