﻿using Devox.PDT.Application.Reference.Commands;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.References
{
    public class CommandTests : TestBase
    {
        [Fact]
        public async Task CreateReferenceTest_Positive()
        {
            var handler = new CreateReference.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.References.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotContain(c => c.Name.Equals("test"));

            //Act
            var result = await handler.Handle(new CreateReference { Name = "test", Link="test" }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.References.ToList();
            entitiesAfter.Count.ShouldBe(4);
            entitiesAfter.ShouldContain(c => c.Name.Equals("test"));
        }
    
        [Fact]
        public async Task UpdateReferenceTest_Positive()
        {
            var handler = new UpdateReference.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var reference = context.References.Where(r => r.Id == 10).FirstOrDefault();
            reference.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(
                new UpdateReference { Id=10, Name = "updateTest", TopicId = 20 }, CancellationToken.None);

            //Assert
            reference.TopicId.ShouldBe(20);
            reference.Name.ShouldBe("updateTest");
        }
        [Fact]
        public async Task UpdateReferenceTest_Negative_ReferenceNotFound()
        {
            var handler = new UpdateReference.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.References.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new UpdateReference { Id= -1, Name = "updatedName", TopicId = 10 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }

        [Fact]
        public async Task DeleteReferenceTest_Positive()
        {
            var handler = new DeleteReference.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.References.Where(t => t.Id == 30).Count().ShouldNotBe(0);
            context.References.Count().ShouldBe(3);
            
            //Act
            await handler.Handle(new DeleteReference { Id = 30 }, CancellationToken.None);

            //Assert
            context.References.Where(t=>t.Id==30).Count().ShouldBe(0);
            context.References.Count().ShouldBe(2);
        }

        [Fact]
        public async Task DeleteReferenceTest_Negative_ReferenceNotFound()
        {
            var handler = new DeleteReference.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.References.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new DeleteReference { Id = -1}, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }


    }
}
