﻿using AutoMapper;
using Devox.PDT.Application.Infrastructure;
using System;

namespace Devox.PDT.Application.Tests.Helpers
{
    public static class AutomapperHelper
    {
        private readonly static Lazy<IMapper> mapper = new Lazy<IMapper>(Init);
        private static MapperConfiguration config;

        public static IMapper Mapper => mapper.Value;

        private static IMapper Init()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutomapperConfig>();
            });

            return new Mapper(config);
        }
    }

}
