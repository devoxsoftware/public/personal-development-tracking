﻿using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Application.Specialization.Queries;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Specializations
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetSpecializationsTest_Positive()
        {
            var handler = new GetSpecializations.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetSpecializations { }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Specializations.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<SpecializationsListView>();
        }

        [Fact]
        public async Task GetSpecializationByIdTest_Exist_Positive()
        {
            var handler = new GetSpecializationById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            entitiesBefore.Count.ShouldBe(3);
            var Specialization = context.Specializations.FirstOrDefault();
            Specialization.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetSpecializationById { Id = Specialization.Id }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Specializations.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<SpecializationModel>();
            result.Id.ShouldBe(Specialization.Id);
            result.Name.ShouldBe(Specialization.Name);
        }

        [Fact]
        public async Task GetSpecializationByIdTest_NonExist_Negative()
        {
            var handler = new GetSpecializationById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            entitiesBefore.Count.ShouldBe(3);
            int nonExistentId = -999;

            //Act
            var result = await handler.Handle(new GetSpecializationById { Id = nonExistentId }, CancellationToken.None);

            //Assert
            result.ShouldBeNull();
            var entitiesAfter = context.Specializations.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
        }
    }
}
