﻿using Devox.PDT.Application.Specialization.Commands;
using Devox.PDT.Application.Specialization.Models;
using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Specializations
{
    public class SpecializationTests : TestBase
    {
        [Fact]
        public async Task CreateSpecializationTest_Positive()
        {
            var handler = new CreateSpecialization.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotContain(c => c.Name.Equals("test"));

            //Act
            var result = await handler.Handle(new CreateSpecialization { Name = "test" }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Specializations.ToList();
            entitiesAfter.Count.ShouldBe(4);
            entitiesAfter.ShouldContain(c => c.Name.Equals("test"));
            result.ShouldBeOfType<SpecializationModel>();
        }

        [Fact]
        public async Task CreateSpecializationTest_Negative_The_Same_Name()
        {
            var handler = new CreateSpecialization.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var countBefore = context.Specializations.Count();
            var entityWhichExists = context.Specializations.First();
            //Act
            Func<Task> result = async () => await handler.Handle(
                new CreateSpecialization { Name = entityWhichExists.Name }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(ValidationException));
            var countAfter = context.Specializations.Count();
            countBefore.ShouldBe(countAfter);
        }

        [Fact]
        public async Task DeleteSpecializationTest_Exist_Pisitive()
        {
            var handler = new DeleteSpecialization.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            var Specialization = context.Specializations.FirstOrDefault();
            entitiesBefore.Count.ShouldBe(3);

            //Act
            var result = await handler.Handle(new DeleteSpecialization { Id = Specialization.Id }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Specializations.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count - 1);
            entitiesAfter.ShouldNotContain(g => g.Id == Specialization.Id);
            result.ShouldBe(Specialization.Id);
        }

        [Fact]
        public async Task DeleteSpecializationTest_NonExist_Negative()
        {
            var handler = new DeleteSpecialization.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            int deleteId = 11;
            entitiesBefore.Count.ShouldBe(3);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new DeleteSpecialization { Id = deleteId }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
            var entitiesAfter = context.Specializations.ToList();
            entitiesBefore.Count.ShouldBe(entitiesAfter.Count);
        }

        [Fact]
        public async Task UpdateSpecializationTest_Exist_Positive()
        {
            var handler = new UpdateSpecialization.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            var Specialization = context.Specializations.FirstOrDefault();
            string updateName = "test";
            entitiesBefore.Count.ShouldBe(3);
            Specialization.ShouldNotBeNull();
            //Act
            var result = await handler.Handle(
                new UpdateSpecialization { Id = Specialization.Id, Name = updateName }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Specializations.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            entitiesAfter.Where(g => g.Id == Specialization.Id).ShouldContain(g => g.Name == updateName);
            result.ShouldBeOfType<SpecializationModel>();
        }

        [Fact]
        public async Task UpdateSpecializationTest_NonExist_Negative()
        {
            var handler = new UpdateSpecialization.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Specializations.ToList();
            int deleteId = 11;
            string updateName = "test";
            entitiesBefore.Count.ShouldBe(3);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new UpdateSpecialization { Id = deleteId, Name = updateName }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
            var entitiesAfter = context.Specializations.ToList();
            entitiesBefore.Count.ShouldBe(entitiesAfter.Count);
        }
    }
}
