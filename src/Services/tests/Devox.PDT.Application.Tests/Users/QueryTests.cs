﻿using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.User.Queries;
using Devox.PDT.Application.User.Commands;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Devox.PDT.Application.User.Models;

namespace Devox.PDT.Application.Tests.Users
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetUsersTest_Positive()
        {
            var handler = new GetUsers.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Users.ToList();
            entitiesBefore.Count.ShouldBe(1);
            entitiesBefore.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetUsers { }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Users.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<UserListView>();
        }

        [Fact]
        public async Task GetUserByIdTest_Exist_Positive()
        {
            var handler = new GetUserById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Users.ToList();
            entitiesBefore.Count.ShouldBe(1);
            var User = context.Users.FirstOrDefault();
            User.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetUserById { Id = User.Id }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Users.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
            result.ShouldBeOfType<UserModel>();
            result.Id.ShouldBe(User.Id);
            result.OAuth2Id.ShouldBe(User.OAuth2Id);
        }

        [Fact]
        public async Task GetUserByOAuth2IdTest_NonExist_Negative()
        {
            var handler = new GetUserByOAuth2Id.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Users.ToList();
            entitiesBefore.Count.ShouldBe(1);
            string nonExistentId = "negative_test";

            //Act
            var result = await handler.Handle(new GetUserByOAuth2Id { OAuth2Id = nonExistentId }, CancellationToken.None);

            //Assert
            result.ShouldBeNull();
            var entitiesAfter = context.Users.ToList();
            entitiesAfter.Count.ShouldBe(entitiesBefore.Count);
        }

    }
}
