﻿using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.Topic.Commands;
using Devox.PDT.Application.User.Commands;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Users
{
    public class CommandTests : TestBase
    {
        [Fact]
        public async Task CreateUserTest_Positive()
        {
            var handler = new CreateUser.Handler(context);

            //Arrange
            var entitiesBefore = context.Users.ToList();
            entitiesBefore.ShouldNotContain(c => c.FirstName.Equals("Test"));
            entitiesBefore.Count.ShouldBe(1);

            //Act
            var result = await handler.Handle(new CreateUser { 
                OAuth2Id = "testId",
                Email="user@devoxsoftware.com",
                FirstName = "Test",
                LastName = "Testovich",
                PictureUrl ="",
                Metadata = new Dictionary<string,string>()

            }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Users.ToList();
            entitiesAfter.ShouldContain(c => c.OAuth2Id.Equals("testId"));
            entitiesAfter.Count.ShouldBe(2);
        }

        [Fact]
        public async Task CreateUserTest_Negative_UserAlreadyExists()
        {
            var handler = new CreateUser.Handler(context);

            //Arrange
            var user = context.Users.FirstOrDefault();

            //Act
            Func<Task> result = async () => 
            await handler.Handle(
                new CreateUser { OAuth2Id = user.OAuth2Id }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(ValidationException));
        }



    }
}
