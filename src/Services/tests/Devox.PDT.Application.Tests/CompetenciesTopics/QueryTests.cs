﻿using Devox.PDT.Application.CompetencyTopic.Queries;
using Devox.PDT.Application.Tests.Helpers;
using Shouldly;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.CompetenciesTopics
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetAllTopicsTest_Positive()
        {
            var handler = new GetAllCompetenciesTopics.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var topicCount = context.CompetenciesTopics.Count();

            //Act
            var result = await handler.Handle(new GetAllCompetenciesTopics { }, CancellationToken.None);

            //Assert
            result.CompetenciesTopics.Count().ShouldBe(topicCount);
        }

        [Fact]
        public async Task GetCompetencyTopicByIdTest_Positive()
        {
            var handler = new GetCompetencyTopicById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var competencyTopic = context.CompetenciesTopics.FirstOrDefault();
            competencyTopic.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetCompetencyTopicById { Id = competencyTopic.Id }, CancellationToken.None);

            //Assert
            result.Id.ShouldBe(competencyTopic.Id);
            result.TopicId.ShouldBe(competencyTopic.TopicId);
            result.CompetencyId.ShouldBe(competencyTopic.CompetencyId);
        }

        [Fact]
        public async Task GetCompetencyTopicById_Negative_CompetencyTopicNotFound()
        {
            var handler = new GetCompetencyTopicById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Competencies.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            var result = await handler.Handle(
                new GetCompetencyTopicById { Id = -1}, CancellationToken.None);

            //Assert
            result.ShouldBe(null);
        }

        

    }
}
