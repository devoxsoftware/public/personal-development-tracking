﻿using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.CompetencyTopic.Commands;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.CompetenciesTopics
{
    public class CommandTests : TestBase
    {
        [Fact]
        public async Task CreateCompetencyTopicTest_Positive()
        {
            var handler = new CreateCompetencyTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.CompetenciesTopics.ToList();
            entitiesBefore.Count.ShouldBe(4);
            entitiesBefore.ShouldNotContain(c => c.CompetencyId == 40 && c.TopicId == 40);

            //Act
            var result = await handler.Handle(
                new CreateCompetencyTopic {  CompetencyId = 40, TopicId = 40 }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.CompetenciesTopics.ToList();
            entitiesAfter.Count.ShouldBe(5);
            entitiesAfter.ShouldContain(c => c.CompetencyId == 40 && c.TopicId == 40);
        }

        [Fact]
        public async Task CreateCompetencyTopicTest_Negative_CompetencyExists()
        {
            var handler = new CreateCompetencyTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.CompetenciesTopics.Where(c => c.CompetencyId == 20 && c.TopicId == 20).Count().ShouldNotBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new CreateCompetencyTopic { CompetencyId = 20, TopicId = 20 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(ValidationException));
        }

        [Fact]
        public async Task UpdateCompetencyTopicTest_Positive()
        {
            var handler = new UpdateCompetencyTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var competency = context.CompetenciesTopics.Where(t => t.Id == 10).FirstOrDefault();
            competency.ShouldNotBeNull();
            
            //Act
            var result = await handler.Handle(
                new UpdateCompetencyTopic { Id = 10, CompetencyId = 50, TopicId = 50 }, CancellationToken.None);

            //Assert
            competency.CompetencyId.ShouldBe(50);
            competency.TopicId.ShouldBe(50);
        }
        [Fact]
        public async Task UpdateCompetencyTopicTest_Negative_CompetencyNotFound()
        {
            var handler = new UpdateCompetencyTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.CompetenciesTopics.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new UpdateCompetencyTopic { Id = -1, CompetencyId = 40, TopicId = 40 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }

        [Fact]
        public async Task DeleteCompetencyTopicTest_Positive()
        {
            var handler = new DeleteCompetencyTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.CompetenciesTopics.Where(t => t.Id == 10).Count().ShouldNotBe(0);
            context.CompetenciesTopics.Count().ShouldBe(4);

            //Act
            await handler.Handle(new DeleteCompetencyTopic { Id = 10 }, CancellationToken.None);

            //Assert
            context.CompetenciesTopics.Where(t => t.Id == 10).Count().ShouldBe(0);
            context.CompetenciesTopics.Count().ShouldBe(3);
        }

        [Fact]
        public async Task DeleteCompetencyTopicTest_Negative_CompetencyTopicNotFound()
        {
            var handler = new DeleteCompetencyTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.CompetenciesTopics.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new DeleteCompetencyTopic { Id = -1 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }


    }
}
