﻿using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.Competency.Queries;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Competencies
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetAllCompetenciesTest_Positive()
        {
            var handler = new GetAllCompetencies.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var competencyCount = context.Competencies.Count();

            //Act
            var result = await handler.Handle(new GetAllCompetencies { }, CancellationToken.None);

            //Assert
            result.Competencies.Count().ShouldBe(competencyCount);
        }

        [Fact]
        public async Task GetCompetencyByIdTest_Positive()
        {
            var handler = new GetCompetencyById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var competency = context.Competencies.FirstOrDefault();
            competency.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetCompetencyById { Id = competency.Id }, CancellationToken.None);

            //Assert
            result.Id.ShouldBe(competency.Id);
            result.SeniorityId.ShouldBe(competency.SeniorityId);
            result.SpecializationId.ShouldBe(competency.SpecializationId);
        }

        [Fact]
        public async Task GetCompetencyById_Negative_CompetencyNotFound()
        {
            var handler = new GetCompetencyById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Competencies.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            var result = await handler.Handle(
                new GetCompetencyById { Id = -1}, CancellationToken.None);

            //Assert
            result.ShouldBe(null);
        }

        

    }
}
