﻿using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.Competency.Commands;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Competencies
{
    public class CommandTests : TestBase
    {
        [Fact]
        public async Task CreateCompetencyTest_Positive()
        {
            var handler = new CreateCompetency.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Competencies.ToList();
            entitiesBefore.Count.ShouldBe(3);
            entitiesBefore.ShouldNotContain(c => c.SeniorityId == 40 && c.SpecializationId == 40);

            //Act
            var result = await handler.Handle(
                new CreateCompetency {  SeniorityId = 40, SpecializationId = 40 }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Competencies.ToList();
            entitiesAfter.Count.ShouldBe(4);
            entitiesAfter.ShouldContain(c => c.SeniorityId == 40 && c.SpecializationId == 40);
        }

        [Fact]
        public async Task CreateCompetencyTest_Negative_CompetencyExists()
        {
            var handler = new CreateCompetency.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Competencies.Where(c => c.SeniorityId == 20 && c.SpecializationId == 20).Count().ShouldNotBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new CreateCompetency { SeniorityId = 20, SpecializationId = 20 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(ValidationException));
        }

        [Fact]
        public async Task UpdateCompetencyTest_Positive()
        {
            var handler = new UpdateCompetency.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var competency = context.Competencies.Where(t => t.Id == 10).FirstOrDefault();
            competency.ShouldNotBeNull();
            
            //Act
            var result = await handler.Handle(
                new UpdateCompetency { Id = 10, SeniorityId = 50, SpecializationId = 50 }, CancellationToken.None);

            //Assert
            competency.SeniorityId.ShouldBe(50);
            competency.SpecializationId.ShouldBe(50);
        }
        [Fact]
        public async Task UpdateCompetencyTest_Negative_CompetencyNotFound()
        {
            var handler = new UpdateCompetency.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Competencies.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new UpdateCompetency { Id = -1, SeniorityId = 40, SpecializationId = 40}, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }

        [Fact]
        public async Task DeleteCompetencyTest_Positive()
        {
            var handler = new DeleteCompetency.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Competencies.Where(t => t.Id == 10).Count().ShouldNotBe(0);
            context.Competencies.Count().ShouldBe(3);

            //Act
            await handler.Handle(new DeleteCompetency { Id = 10 }, CancellationToken.None);

            //Assert
            context.Competencies.Where(t => t.Id == 10).Count().ShouldBe(0);
            context.Competencies.Count().ShouldBe(2);
        }

        [Fact]
        public async Task DeleteCompetencyTest_Negative_CompetencyNotFound()
        {
            var handler = new DeleteCompetency.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Competencies.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new DeleteCompetency { Id = -1 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }


    }
}
