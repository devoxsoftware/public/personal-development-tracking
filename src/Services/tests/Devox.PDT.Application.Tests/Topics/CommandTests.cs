﻿using Devox.PDT.Application.Infrastructure.Exceptions;
using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.Topic.Commands;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Topics
{
    public class CommandTests : TestBase
    {
        [Fact]
        public async Task CreateTopicTest_Positive()
        {
            var handler = new CreateTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var entitiesBefore = context.Topics.ToList();
            entitiesBefore.Count.ShouldBe(4);
            entitiesBefore.ShouldNotContain(c => c.Name.Equals("test"));

            //Act
            var result = await handler.Handle(new CreateTopic { Name = "test" }, CancellationToken.None);

            //Assert
            var entitiesAfter = context.Topics.ToList();
            entitiesAfter.Count.ShouldBe(5);
            entitiesAfter.ShouldContain(c => c.Name.Equals("test"));
        }

        [Fact]
        public async Task UpdateTopicTest_Positive()
        {
            var handler = new UpdateTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var topic = context.Topics.Where(t => t.Id == 10).FirstOrDefault();
            topic.ShouldNotBeNull();
            
            //Act
            var result = await handler.Handle(
                new UpdateTopic { Id = 10, Name = "updateTest", GroupId = 20 }, CancellationToken.None);

            //Assert
            topic.GroupId.ShouldBe(20);
            topic.Name.ShouldBe("updateTest");
        }
        [Fact]
        public async Task UpdateTopicTest_Negative_TopicNotFound()
        {
            var handler = new UpdateTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Topics.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new UpdateTopic { Id = -1, Name = "updatedName", GroupId = 10 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }

        [Fact]
        public async Task DeleteTopicTest_Positive()
        {
            var handler = new DeleteTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Topics.Where(t => t.Id == 10).Count().ShouldNotBe(0);

            //Act
            await handler.Handle(new DeleteTopic { Id = 10 }, CancellationToken.None);

            //Assert
            context.Topics.Where(t => t.Id == 10).Count().ShouldBe(0);
        }

        [Fact]
        public async Task DeleteTopicTest_Negative_TopicNotFound()
        {
            var handler = new DeleteTopic.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Topics.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            Func<Task> result = async () => await handler.Handle(
                new DeleteTopic { Id = -1 }, CancellationToken.None);

            //Assert
            await result.ShouldThrowAsync(typeof(NotFoundException));
        }


    }
}
