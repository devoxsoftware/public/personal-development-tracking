﻿using Devox.PDT.Application.Tests.Helpers;
using Devox.PDT.Application.Topic.Queries;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Devox.PDT.Application.Tests.Topics
{
    public class QueryTests : TestBase
    {
        [Fact]
        public async Task GetAllTopicsTest_Positive()
        {
            var handler = new GetAllTopics.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var topicCount = context.Topics.Count();

            //Act
            var result = await handler.Handle(new GetAllTopics {}, CancellationToken.None);

            //Assert
            result.Topics.Count().ShouldBe(topicCount);
        }

        [Fact]
        public async Task GetTopicByIdTest_Positive()
        {
            var handler = new GetTopicById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            var topic = context.Topics.FirstOrDefault();
            topic.ShouldNotBeNull();

            //Act
            var result = await handler.Handle(new GetTopicById { Id = topic.Id }, CancellationToken.None);

            //Assert
            result.Id.ShouldBe(topic.Id);
            result.Name.ShouldBe(topic.Name);
            result.GroupId.ShouldBe(topic.GroupId);
        }

        [Fact]
        public async Task GetTopicById_Negative_TopicNotFound()
        {
            var handler = new GetTopicById.Handler(context, AutomapperHelper.Mapper);

            //Arrange
            context.Topics.Where(t => t.Id == -1).Count().ShouldBe(0);

            //Act
            var result = await handler.Handle(
                new GetTopicById { Id = -1}, CancellationToken.None);

            //Assert
            result.ShouldBe(null);
        }

        [Fact]
        public async Task GetTopicByCompetencyIdTest_Positive()
        {
            var handler = new GetTopicsByCompetencyId.Handler(context, AutomapperHelper.Mapper);

            //Arrange
             
            //Act
            var result = await handler.Handle(new GetTopicsByCompetencyId { CompetencyId = 30 }, CancellationToken.None);

            //Assert
            result.Topics.Count().ShouldBe(2);
        }

        [Fact]
        public async Task GetTopicByGroupIdTest_Positive()
        {
            var handler = new GetTopicsByGroupId.Handler(context, AutomapperHelper.Mapper);

            //Arrange

            //Act
            var result = await handler.Handle(new GetTopicsByGroupId { GroupId = 30 }, CancellationToken.None);

            //Assert
            result.Topics.Count().ShouldBe(2);
        }

    }
}
