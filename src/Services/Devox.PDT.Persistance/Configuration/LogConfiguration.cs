﻿using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Devox.PDT.Persistance.Configuration
{
    public class LogConfiguration : IEntityTypeConfiguration<Log>
    {
        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.ToTable(nameof(Log));

            builder.Property(e => e.Level)
                .HasMaxLength(128);

            builder.Property(e => e.TimeStamp)
                .IsRequired();
        }
    }
}
