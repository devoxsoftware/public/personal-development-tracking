﻿using Microsoft.EntityFrameworkCore;
using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Devox.PDT.Persistance.Configuration
{
    class SeniorityConfiguration : IEntityTypeConfiguration<Seniority>
    {
        public void Configure(EntityTypeBuilder<Seniority> builder)
        {
            builder.ToTable(nameof(Seniority));

            builder.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd();

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasMaxLength(50);

            builder.HasQueryFilter(m => !m.IsDeleted);
        }
    }
}
