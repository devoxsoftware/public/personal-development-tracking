﻿using Microsoft.EntityFrameworkCore;
using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Devox.PDT.Persistance.Configuration
{
    class CompetencyTopicConfiguration : IEntityTypeConfiguration<CompetencyTopic>
    {
        public void Configure(EntityTypeBuilder<CompetencyTopic> builder)
        {
            builder.ToTable(nameof(CompetencyTopic));

            builder.Property(e => e.Id)
                .HasColumnName("Id")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.CompetencyId).HasColumnName("CompetencyId");

            builder.Property(e => e.TopicId).HasColumnName("TopicId");

            builder.HasOne(d => d.Competency)
                .WithMany(p => p.CompetencyTopic)
                .HasForeignKey(d => d.CompetencyId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK_CompetencyTopic_Competency");

            builder.HasOne(d => d.Topic)
                .WithMany(p => p.CompetencyTopic)
                .HasForeignKey(d => d.TopicId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK_CompetencyTopic_Topic");

            builder.HasQueryFilter(m => !m.IsDeleted);
        }
    }
}
