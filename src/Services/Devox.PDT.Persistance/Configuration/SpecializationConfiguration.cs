﻿using Microsoft.EntityFrameworkCore;
using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Devox.PDT.Persistance.Configuration
{
    class SpecializationConfiguration : IEntityTypeConfiguration<Specialization>
    {
        public void Configure(EntityTypeBuilder<Specialization> builder)
        {
            builder.ToTable(nameof(Specialization));

            builder.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd();

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasMaxLength(50);

            builder.HasQueryFilter(m => !m.IsDeleted);
        }
    }
}
