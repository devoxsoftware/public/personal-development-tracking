﻿using Microsoft.EntityFrameworkCore;
using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Devox.PDT.Persistance.Configuration
{
    class ReferenceConfiguration : IEntityTypeConfiguration<Reference>
    {
        public void Configure(EntityTypeBuilder<Reference> builder)
        {
            builder.ToTable(nameof(Reference));

            builder.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd();

            builder.Property(e => e.Link)
                .IsRequired()
                .HasColumnName("Link");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasMaxLength(50);

            builder.Property(e => e.TopicId).HasColumnName("TopicId");

            builder.HasOne(d => d.Topic)
                .WithMany(p => p.Reference)
                .HasForeignKey(d => d.TopicId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK_Reference_Topic");

            builder.HasQueryFilter(m => !m.IsDeleted);
        }
    }
}
