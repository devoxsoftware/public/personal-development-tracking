﻿using Microsoft.EntityFrameworkCore;
using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Devox.PDT.Persistance.Configuration
{
    class TopicConfiguration : IEntityTypeConfiguration<Topic>
    {
        public void Configure(EntityTypeBuilder<Topic> builder)
        {
            builder.ToTable(nameof(Topic));

            builder.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd();

            builder.Property(e => e.GroupId).HasColumnName("GroupId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasMaxLength(50);

            builder.HasOne(d => d.Group)
                .WithMany(p => p.Topic)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK_Topic_Group");

            builder.HasQueryFilter(m => !m.IsDeleted);
        }
    }
}
