﻿using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Devox.PDT.Persistance.Configuration
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));
            builder.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd();

            builder.Property(e => e.FirstName)
                .HasColumnName("FirstName")
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(e => e.LastName)
               .HasColumnName("LastName")
               .IsRequired()
               .HasMaxLength(150);

            builder.Property(e => e.Email)
                .IsRequired()
                .HasColumnName("Email")
                .HasMaxLength(150);

            builder.Property(e => e.PictureUrl)
                .HasColumnName("PictureUrl")
                .HasMaxLength(300);            

            builder.Property(e => e.OAuth2Id)
               .HasColumnName("OAuth2Id")
               .HasMaxLength(150);

            builder.Property(e => e.Metadata)
                .HasConversion( v => JsonConvert.SerializeObject(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                v => JsonConvert.DeserializeObject<IDictionary<string, string>>(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
            
        }
    }
}
