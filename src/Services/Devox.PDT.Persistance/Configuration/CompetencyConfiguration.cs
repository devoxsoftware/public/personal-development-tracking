﻿using Microsoft.EntityFrameworkCore;
using Devox.PDT.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Devox.PDT.Persistance.Configuration
{
    class CompetencyConfiguration : IEntityTypeConfiguration<Competency>
    { 
        public void Configure(EntityTypeBuilder<Competency> builder)
        {
            builder.ToTable(nameof(Competency));
            builder.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd();

            builder.Property(e => e.SeniorityId).HasColumnName("SeniorityId");

            builder.Property(e => e.SpecializationId).HasColumnName("SpecializationId");

            builder.HasOne(d => d.Seniority)
                        .WithMany(p => p.Competency)
                        .HasForeignKey(d => d.SeniorityId)
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("FK_Competency_Seniority");

            builder.HasOne(d => d.Specialization)
                        .WithMany(p => p.Competency)
                        .HasForeignKey(d => d.SpecializationId)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("FK_Competency_Specialization");

            builder.HasQueryFilter(m => !m.IsDeleted);
        }
    }
}
