﻿using Devox.PDT.Domain.Entities;
using Devox.PDT.Infrastructure.CurrentUser;
using Devox.PDT.Persistance.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Devox.PDT.Persistance
{
    public class PdtDbContext : DbContext
    {
        private readonly IUserContext userContext;
        public PdtDbContext(DbContextOptions<PdtDbContext> options, IUserContext userContext)
            : base(options)
        {
            this.userContext = userContext;
        }
        public virtual DbSet<Competency> Competencies { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<Reference> References { get; set; }
        public virtual DbSet<CompetencyTopic> CompetenciesTopics { get; set; }
        public virtual DbSet<Seniority> Seniorities { get; set; }
        public virtual DbSet<Specialization> Specializations { get; set; }
        public virtual DbSet<Topic> Topics { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CompetencyTopicConfiguration());
            modelBuilder.ApplyConfiguration(new CompetencyConfiguration());
            modelBuilder.ApplyConfiguration(new GroupConfiguration());
            modelBuilder.ApplyConfiguration(new ReferenceConfiguration());
            modelBuilder.ApplyConfiguration(new SeniorityConfiguration());
            modelBuilder.ApplyConfiguration(new SpecializationConfiguration());
            modelBuilder.ApplyConfiguration(new TopicConfiguration());
            modelBuilder.ApplyConfiguration(new LogConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            foreach (var entityEntry in ChangeTracker.Entries().Where(it => it.Entity is EntityBase)
                .Where(it => it.State == EntityState.Modified || 
                it.State == EntityState.Added || it.State == EntityState.Deleted))
            {
                var entity = (EntityBase)entityEntry.Entity;
                switch (entityEntry.State) 
                {
                    case EntityState.Added:
                        entity.Created = DateTime.UtcNow;
                        entity.CreatedBy = userContext.Id;
                        break;
                    case EntityState.Modified:
                        entity.Updated = DateTime.UtcNow;
                        entity.UpdatedBy = userContext.Id;
                        break;
                    case EntityState.Deleted:
                        entity.IsDeleted = true;
                        entityEntry.State = EntityState.Modified;
                        break;
                }
            }
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        public void Seed()
        {
            Users.Add(new User 
            {
                OAuth2Id = "system",
                Email = "system@devoxsoftware.com",
                FirstName = "System",
                LastName = "System",
                PictureUrl = "",
                Metadata = new Dictionary<string, string>()
            });

            Specializations.AddRange(new Specialization[] {
                new Specialization
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Asp.Net Core"
                },
                new Specialization
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "QA"
                },
                new Specialization
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Python"
                } });

            SaveChanges();
            Seniorities.AddRange(new Seniority[] {
                new Seniority
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Junior"

                },
                new Seniority
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Middle"

                },
                new Seniority
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Senior"

                } });

            SaveChanges();
            Competencies.AddRange(new Competency[] {
                new Competency
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    SeniorityId = 1,
                    SpecializationId = 1
                },
                new Competency
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    SeniorityId = 2,
                    SpecializationId = 2
                },
                new Competency
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    SeniorityId = 3,
                    SpecializationId = 3
                } });

            SaveChanges();

            Groups.AddRange(new Group[] { 
                new Group
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Web developement"
                },
                new Group
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Testing"
                },
                new Group
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Python"
                } });

            SaveChanges();

            Topics.AddRange(new Topic[]{
                new Topic
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Asp.Net",
                    GroupId = 1
                },
                new Topic
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Testing",
                    GroupId = 2
                },
                new Topic
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Name = "Python language",
                    GroupId = 3

                } });

            SaveChanges();
            CompetenciesTopics.AddRange(new CompetencyTopic[] {
                new CompetencyTopic
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    CompetencyId = 1,
                    TopicId = 1
                },
                new CompetencyTopic
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    CompetencyId = 2,
                    TopicId = 2
                },
                new CompetencyTopic
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    CompetencyId = 3,
                    TopicId = 3
                } });
            SaveChanges();
           
            References.AddRange(new Reference[] {
                new Reference
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Link = "google.com",
                    Name = "google",
                    TopicId = 1

                },
                new Reference
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Link = "google.com",
                    Name = "google",
                    TopicId = 2

                },
                new Reference
                {
                    Created = DateTime.UtcNow,
                    CreatedBy = 1,
                    Link = "google.com",
                    Name = "google",
                    TopicId = 3

                } });

            SaveChanges();
           

        }

    }
}
