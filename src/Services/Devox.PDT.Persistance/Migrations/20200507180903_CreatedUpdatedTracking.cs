﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Devox.PDT.Persistance.Migrations
{
    public partial class CreatedUpdatedTracking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Topic",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Topic",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Topic",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Topic",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Topic",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Specialization",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Specialization",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Specialization",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Specialization",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Specialization",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Seniority",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Seniority",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Seniority",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Seniority",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Seniority",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Reference",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Reference",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Reference",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Reference",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Reference",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Log",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Log",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Log",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Log",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Log",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Group",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Group",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Group",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "CompetencyTopic",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "CompetencyTopic",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "CompetencyTopic",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "CompetencyTopic",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "CompetencyTopic",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Competency",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "Competency",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Competency",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Competency",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Competency",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "Topic");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Topic");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Topic");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Topic");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Topic");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Specialization");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Specialization");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Specialization");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Specialization");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Specialization");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Seniority");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Seniority");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Seniority");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Seniority");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Seniority");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Reference");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Reference");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Reference");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Reference");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Reference");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Log");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Log");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Log");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Log");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Log");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "CompetencyTopic");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "CompetencyTopic");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "CompetencyTopic");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "CompetencyTopic");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "CompetencyTopic");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Competency");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Competency");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Competency");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Competency");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Competency");
        }
    }
}
