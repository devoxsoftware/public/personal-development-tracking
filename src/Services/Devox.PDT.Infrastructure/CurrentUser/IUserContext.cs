﻿using System.Collections.Generic;

namespace Devox.PDT.Infrastructure.CurrentUser
{
    public interface IUserContext
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        string PictureUrl { get; set; }
        string OAuth2Id { get; set; }
        IDictionary<string, string> Metadata { get; set; }
    }
}
