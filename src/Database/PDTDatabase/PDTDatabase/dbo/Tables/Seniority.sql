﻿CREATE TABLE [dbo].[Seniority] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Seniority] PRIMARY KEY CLUSTERED ([Id] ASC)
);



