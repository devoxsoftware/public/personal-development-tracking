﻿CREATE TABLE [dbo].[Topic] (
    [Id]      INT           IDENTITY (1, 1) NOT NULL,
    [Name]    NVARCHAR (50) NOT NULL,
    [GroupId] INT           NOT NULL,
    CONSTRAINT [PK_Topic] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Topic_Group] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Group] ([Id]) ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX [IX_Topic_groupId]
    ON [dbo].[Topic]([groupId] ASC);

