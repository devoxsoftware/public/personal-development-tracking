﻿CREATE TABLE [dbo].[Reference] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]    NVARCHAR (50)  NOT NULL,
    [TopicId] INT            NOT NULL,
    [Link]    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Reference] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Reference_Topic] FOREIGN KEY ([TopicId]) REFERENCES [dbo].[Topic] ([Id]) ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX [IX_Reference_topicId]
    ON [dbo].[Reference]([topicId] ASC);

