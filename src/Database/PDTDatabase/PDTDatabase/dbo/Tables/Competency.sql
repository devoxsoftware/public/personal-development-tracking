﻿CREATE TABLE [dbo].[Competency] (
    [Id]               INT IDENTITY (1, 1) NOT NULL,
    [SpecializationId] INT NOT NULL,
    [SeniorityId]      INT NOT NULL,
    CONSTRAINT [PK_Competency] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Competency_Seniority] FOREIGN KEY ([SeniorityId]) REFERENCES [dbo].[Seniority] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Competency_Specialization] FOREIGN KEY ([SpecializationId]) REFERENCES [dbo].[Specialization] ([Id]) ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX [IX_Competency_specializationId]
    ON [dbo].[Competency]([specializationId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Competency_SeniorityId]
    ON [dbo].[Competency]([SeniorityId] ASC);

