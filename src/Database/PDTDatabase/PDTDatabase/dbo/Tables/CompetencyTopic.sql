﻿CREATE TABLE [dbo].[CompetencyTopic] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [CompetencyId] INT NOT NULL,
    [TopicId]      INT NOT NULL,
    CONSTRAINT [PK_CompetencyTopic] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompetencyTopic_Competency] FOREIGN KEY ([CompetencyId]) REFERENCES [dbo].[Competency] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_CompetencyTopic_Topic] FOREIGN KEY ([TopicId]) REFERENCES [dbo].[Topic] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_CompetencyTopic_TopicId]
    ON [dbo].[CompetencyTopic]([TopicId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CompetencyTopic_CompetencyId]
    ON [dbo].[CompetencyTopic]([CompetencyId] ASC);

