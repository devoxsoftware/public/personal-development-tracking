﻿CREATE TABLE [dbo].[User] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [FirstName]  NVARCHAR (150)  NOT NULL,
    [LastName]   NVARCHAR (150)  NOT NULL,
    [Email]      NVARCHAR (150)  NOT NULL,
    [PictureUrl] NVARCHAR (300)  NULL,
    [OAuth2Id]   NVARCHAR (150)  NULL,
    [Metadata]   NVARCHAR (1500) NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC)
);



