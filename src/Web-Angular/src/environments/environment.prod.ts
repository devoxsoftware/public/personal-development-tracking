export const environment = {
  production: true,
  baseUrl: 'https://localhost:44392/api',
  callbackURL: 'http://localhost:4200',
};
