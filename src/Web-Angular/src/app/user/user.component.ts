import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { PdtHttpClient } from '../services/pdt-api/pdt-http-client';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public profile: any;

  constructor(public auth: AuthService, private http: PdtHttpClient) {
  }

  ngOnInit() {
    this.http.get<any>('users/current', {
      headers: new HttpHeaders()
        .set('api-version', `1.0`)
        .set('Authorization', `Bearer ${localStorage.getItem('access_token')}`)
    }).subscribe(result => {
      this.profile = result;
    }, error => console.error(error));
  }
}


