import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpHandler, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PdtHttpClient extends HttpClient {
  constructor(httpHandler: HttpHandler) {
    super(httpHandler);  }


  public request(
    first: string | HttpRequest<any>,
    url?: string,
    options: {
      body?: any,
      headers?: HttpHeaders | { [header: string]: string | string[] },
      observe?: 'body' | 'events' | 'response',
      params?: HttpParams | { [param: string]: string | string[] },
      reportProgress?: boolean,
      responseType?: 'arraybuffer' | 'blob' | 'json' | 'text',
      withCredentials?: boolean,
    } = {}
  ): Observable<any> {

    options.withCredentials = false;

    if (first instanceof HttpRequest) {
      return super.request(first as any, url, options);
    } else {
      return super.request(first, environment.baseUrl + url, options);
    }

  }
}
