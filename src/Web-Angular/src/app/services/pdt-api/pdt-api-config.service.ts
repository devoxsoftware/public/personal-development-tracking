import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PdtApiConfigService {
  baseUrl = environment.baseUrl;
  apiPaths = environment.apiPaths;

  constructor() { }
}
