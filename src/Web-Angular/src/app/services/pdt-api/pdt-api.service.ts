import { Injectable } from '@angular/core';
import { Specialization, Seniority, Competency, Topic, Reference, Group } from './pdt-api.model';
import { share } from 'rxjs/operators';
import { PdtApiConfigService } from './pdt-api-config.service';
import { PdtHttpClient } from './pdt-http-client';

@Injectable({
  providedIn: 'root'
})
export class PdtApiService {

  constructor(private http: PdtHttpClient,
              private pdtApiConfig: PdtApiConfigService) { }

  getSpecializations() {
    return this.http.get<Specialization>(this.pdtApiConfig.apiPaths.specializations).pipe(share());
  }

  patchSpecialization(id: number, name: string){
    return this.http.patch<Specialization>(this.pdtApiConfig.apiPaths.specializations,
    {
      id,
      name
    }).pipe(share());
  }

  postSpecialization(name: string){
    return this.http.post<Specialization>(this.pdtApiConfig.apiPaths.specializations,
    {
      name
    }).pipe(share());
  }

  deleteSpecialization(id: number){
    return this.http.delete<Specialization>(this.pdtApiConfig.apiPaths.specializations + `/${id}`).pipe(share());
  }

  getSeniorities() {
    return this.http.get<Seniority>(this.pdtApiConfig.apiPaths.seniorities).pipe(share());
  }

  getSeniorityBySpecialization(id: number) {
    return this.http.get<Seniority>(this.pdtApiConfig.apiPaths.specializations +
                                    `/${id}/${this.pdtApiConfig.apiPaths.seniorities}`).pipe(share());
  }

  patchSeniority(id: number, name: string){
    return this.http.patch<Seniority>(this.pdtApiConfig.apiPaths.seniorities,
    {
      id,
      name
    }).pipe(share());
  }

  postSeniority(name: string){
    return this.http.post<Seniority>(this.pdtApiConfig.apiPaths.seniorities,
    {
      name
    }).pipe(share());
  }

  deleteSeniority(id: number){
    return this.http.delete<Seniority>(this.pdtApiConfig.apiPaths.seniorities +
                                      `/${id}`).pipe(share());
  }

  getGroupsByCompetency(competencyId: number){
    return this.http.get<Group>(this.pdtApiConfig.apiPaths.competencies +
                                `/${competencyId}/${this.pdtApiConfig.apiPaths.groups}`).pipe(share());
  }

  patchGroup(id: number, name: string){
    return this.http.patch<Group>(this.pdtApiConfig.apiPaths.groups,
    {
      id,
      name
    }).pipe(share());
  }

  postGroup(name: string){
    return this.http.post<Group>(this.pdtApiConfig.apiPaths.groups,
    {
      name
    }).pipe(share());
}

  deleteGroup(id: number){
    return this.http.delete<Group>(this.pdtApiConfig.apiPaths.groups +
                                  `/${id}`).pipe(share());
  }


  getCompetencyBySeniorityAndSpecialization(specializationId: number, seniorityId: number){
    return this.http.get<Competency>
    (this.pdtApiConfig.apiPaths.competencies +
                                  `/find?specializationId=${specializationId}&seniorityId=${seniorityId}`).pipe(share());
  }

  postCompetency(SpecializationId: number, SeniorityId: number){
    return this.http.post<Topic>(this.pdtApiConfig.apiPaths.competencies,
    {
      SpecializationId,
      SeniorityId
    }).pipe(share());
  }

  getTopicsByGroup(groupId: number){
    return this.http.get<Topic>(this.pdtApiConfig.apiPaths.groups +
                                `/${groupId}/${this.pdtApiConfig.apiPaths.topics}`).pipe(share());
  }

  patchTopic(id: number, name: string, groupId: number){
    return this.http.patch<Topic>(this.pdtApiConfig.apiPaths.topics,
    {
      id,
      name,
      groupId
    }).pipe(share());
  }

  postTopic(name: string, groupId: number){
    return this.http.post<Topic>(this.pdtApiConfig.apiPaths.topics,
    {
      name,
      groupId
    }).pipe(share());
  }

  deleteTopic(id: number){
    return this.http.delete<Topic>(this.pdtApiConfig.apiPaths.topics +
                                  `/${id}`).pipe(share());

  }

  getReferencesByTopicId(topicId: number){
    return this.http.get<Reference>(this.pdtApiConfig.apiPaths.topics +
                                    `/${topicId}/${this.pdtApiConfig.apiPaths.references}`).pipe(share());
  }

  patchReference(id: number, name: string, TopicId: number, Link: string){
    return this.http.patch<Reference>(this.pdtApiConfig.apiPaths.references,
    {
      id,
      name,
      TopicId,
      Link
    }).pipe(share());
  }

  postReference(name: string, TopicId: number, Link: string){
    return this.http.post<Reference>(this.pdtApiConfig.apiPaths.references,
    {
      name,
      TopicId,
      Link
    }).pipe(share());
  }

  deleteReference(id: number){
    return this.http.delete<Reference>(this.pdtApiConfig.apiPaths.references +
                                      `/${id}`).pipe(share());
  }

  postCompetencyTopic(topicId: number, competencyId: number){
    return this.http.post<any>(this.pdtApiConfig.apiPaths.competencyTopic,
    {
      topicId,
      competencyId
    }).pipe(share());
  }
}
