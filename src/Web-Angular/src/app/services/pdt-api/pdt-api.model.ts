import { Identifiers } from '@angular/compiler';

export interface Specialization {
    id: number;
    name: string;
}

export interface Seniority {
    id: number;
    name: string;
}

export interface Competency {
    id: number;
    specializationId: number;
    seniorityId: number;
}

export interface Group {
    id: number;
    name: string;
}

export interface Topic {
    id: number;
    name: string;
    groupId: number;
}

export interface Reference {
    id: number;
    name: string;
    topicId: number;
    link: string;
}
