import { environment } from 'src/environments/environment';

interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
  apiUrl: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'zSRP6YgHO15FGt0AWFeGFvxmahu5JeNz',
  domain: 'devoxsoftware.eu.auth0.com',
  callbackURL: environment.callbackURL,
  apiUrl: 'https://pdt/api'
};
