import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.auth.isAuthenticated())
    {
        this.auth.login();
    }

    request = request.clone({
      headers: new HttpHeaders({
        'api-version': '1.0',
        Authorization: `Bearer ${this.auth.getToken()}`,
      }),
    });
    if (!environment.production){
      console.log(request);
    }
    return next.handle(request);
  }
}
