import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrackerComponent } from './tracker.component';
import { MaterialModule } from '../material-module/material.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { TopicComponent } from './topic/topic.component';
import { EditorComponent } from './editor/editor.component';
import { SeniorityChoiceDialogComponent } from './seniority-choice-dialog/seniority-choice-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';



@NgModule({
  declarations: [
    TrackerComponent,
    TopicComponent,
    EditorComponent,
    SeniorityChoiceDialogComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class TrackerModule { }
