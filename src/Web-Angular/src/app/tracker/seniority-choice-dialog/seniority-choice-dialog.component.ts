import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, Observable } from 'rxjs';
import { Seniority } from 'src/app/services/pdt-api/pdt-api.model';
import { PdtApiService } from 'src/app/services/pdt-api/pdt-api.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-seniority-choice-dialog',
  templateUrl: './seniority-choice-dialog.component.html',
  styleUrls: ['./seniority-choice-dialog.component.scss']
})
export class SeniorityChoiceDialogComponent implements OnInit {

  choice: Subject<Seniority> = new Subject<Seniority>();
  seniorities = new Observable<Seniority>();
  constructor(public dialogRef: MatDialogRef<SeniorityChoiceDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public dialogData: any,
              private pdtApiService: PdtApiService) { }

  ngOnInit(): void {
    this.seniorities = this.pdtApiService.getSeniorities();
    this.seniorities.subscribe();
  }

  choose(event: Event, choice: Seniority){
    this.choice.next(choice);
    this.choice.complete();
    this.dialogRef.close();
  }
}
