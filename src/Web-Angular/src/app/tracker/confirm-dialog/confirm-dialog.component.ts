import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})

export class ConfirmDialogComponent implements OnInit {

  confirmed: Subject<boolean> = new Subject<boolean>();

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public dialogData: any) { }

  ngOnInit(): void {
  }

  confirm(){
    this.confirmed.next(true);
    this.confirmed.complete();
    this.dialogRef.close();
  }

  cancel(){
    this.confirmed.next(false);
    this.confirmed.complete();
    this.dialogRef.close();
  }
}
