import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Topic, Reference } from 'src/app/services/pdt-api/pdt-api.model';
import { Observable } from 'rxjs';
import { PdtApiService } from 'src/app/services/pdt-api/pdt-api.service';
import { MatDialog } from '@angular/material/dialog';
import { EditorComponent } from '../editor/editor.component';
import { ConfirmDialogComponent } from 'src/app/tracker/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {

  constructor(private pdtApiService: PdtApiService, public dialog: MatDialog) { }

  @Input() topic: Topic;
  @Output() changed = new EventEmitter();

  references: Observable<Reference>;

  ngOnInit(): void {
    this.loadReferences();
  }

  loadReferences(){
    this.references = null;
    this.references = this.pdtApiService.getReferencesByTopicId(this.topic.id);
  }

  editReference(event: Event, reference: Reference ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(EditorComponent, {
      width: '250px',
      data: { type: 'reference', obj: reference, mode: 'patch'}
    });
    dialogRef.componentInstance.dialogResult.subscribe(
      () => {
        this.loadReferences();
        this.changed.next();
      }
      );
    this.changed.next();
  }

  addReference(event: Event){
    event.stopPropagation();
    const newSpecialization: Reference = {name: '', link: '', topicId: this.topic.id} as Reference;
    const dialogRef = this.dialog.open(EditorComponent, {
      width: '250px',
      data: { type: 'reference', obj: newSpecialization, mode: 'post'}
    });
    dialogRef.componentInstance.dialogResult.subscribe(
      () => {
        this.loadReferences();
        this.changed.next();
      }
      );
  }

  deleteReference(event: Event, reference: Reference ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: `Do you really wish to delete ${reference.name} (${reference.link} reference?`}
    });
    dialogRef.componentInstance.confirmed.subscribe(confirmed =>
    {
      if (confirmed)
      {
        this.pdtApiService.deleteReference(reference.id).subscribe(
          () => {
            this.changed.next();
            this.loadReferences();
        });
      }
    });
  }

  editTopic(event: Event, topic: Topic ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(EditorComponent, {
      width: '250px',
      data: { type: 'topic', obj: topic, mode: 'patch'}
    });
    dialogRef.componentInstance.dialogResult.subscribe(
      () => {
        this.changed.next();
      }
    );
  }

  deleteTopic(event: Event, topic: Topic ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: `Do you really wish to delete ${topic.name} topic?`}
    });
    dialogRef.componentInstance.confirmed.subscribe(confirmed =>
    {
      if (confirmed)
      {
        this.pdtApiService.deleteTopic(topic.id).subscribe(
          () => {
            this.loadReferences();
            this.changed.next();
          });
      }
    });
  }

}
