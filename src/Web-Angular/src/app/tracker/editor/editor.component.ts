import { Component, OnInit, Inject, ChangeDetectionStrategy, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PdtApiService } from '../../services/pdt-api/pdt-api.service';
import { Observable, Subject } from 'rxjs';
import { share } from 'rxjs/operators';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})



export class EditorComponent implements OnInit {

  formGroup: FormGroup;
  dialogResult: Subject<any> = new Subject<any>();

  constructor(public dialogRef: MatDialogRef<EditorComponent>,
              @Inject(MAT_DIALOG_DATA) public dialogData: any,
              private formBuilder: FormBuilder,
              private pdtApiService: PdtApiService) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({fields: this.formBuilder.group([''])});
    (this.formGroup.controls.fields as FormGroup).removeControl('0');
    this.parseDialogData();
  }

  parseDialogData(){
    Object.keys(this.dialogData.obj).forEach(key => {
    if (!key.toLowerCase().includes('id')){
      (this.formGroup.get('fields') as FormGroup).addControl(key, this.formBuilder.control(['']));
    }
    });
  }



  onSubmit(){
    let action: Observable<any>;
    switch (this.dialogData.type){
      case 'specialization':
        if (this.dialogData.mode === 'patch'){
        action = this.pdtApiService.patchSpecialization(this.dialogData.obj.id, this.formGroup.get('fields').get('name').value);
        }
        if (this.dialogData.mode === 'post'){
        action = this.pdtApiService.postSpecialization(this.formGroup.get('fields').get('name').value);
        }
        break;
      case 'topic':
        if (this.dialogData.mode === 'patch'){
        action = this.pdtApiService.patchTopic(
          this.dialogData.obj.id,
          this.formGroup.get('fields').get('name').value,
          this.dialogData.obj.groupId
          );
        }
        if (this.dialogData.mode === 'post'){
        action = this.pdtApiService.postTopic(
          this.formGroup.get('fields').get('name').value,
          this.dialogData.obj.groupId
          );
        }
        break;
      case 'reference':
        if (this.dialogData.mode === 'patch'){
        action = this.pdtApiService.patchReference(
          this.dialogData.obj.id,
          this.formGroup.get('fields').get('name').value,
          this.dialogData.obj.topicId,
          this.formGroup.get('fields').get('link').value
          );
        }
        if (this.dialogData.mode === 'post'){
        action = this.pdtApiService.postReference(
          this.formGroup.get('fields').get('name').value,
          this.dialogData.obj.topicId,
          this.formGroup.get('fields').get('link').value
          );
        }
        break;
      case 'group':
        if (this.dialogData.mode === 'patch'){
        action = this.pdtApiService.patchGroup(
          this.dialogData.obj.id,
          this.formGroup.get('fields').get('name').value,
          );
        }
        if (this.dialogData.mode === 'post'){
        action = this.pdtApiService.postGroup(
          this.formGroup.get('fields').get('name').value
          ).pipe(share());
        action.subscribe( res =>
          {
            this.dialogData.obj.id = res.id;
            console.log(res);
          });
        }
        break;
    }
    action.subscribe( result => {this.dialogResult.next(result); });
    this.dialogRef.close();
  }

  getControlNames(){
    const res = Object.keys((this.formGroup.get('fields') as FormGroup).controls);
    return res;
  }

  getControlName(c: AbstractControl): string | null {
    const formGroup = c.parent.controls;
    return Object.keys(formGroup).find(name => c === formGroup[name]) || null;
}
}
