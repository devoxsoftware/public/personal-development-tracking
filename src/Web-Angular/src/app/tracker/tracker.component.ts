import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PdtApiService } from '../services/pdt-api/pdt-api.service';
import { Observable } from 'rxjs';
import { Specialization, Seniority, Competency, Group, Topic } from '../services/pdt-api/pdt-api.model';
import { take, toArray } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { EditorComponent } from './editor/editor.component';
import { ConfirmDialogComponent } from 'src/app/tracker/confirm-dialog/confirm-dialog.component';
import { SeniorityChoiceDialogComponent } from './seniority-choice-dialog/seniority-choice-dialog.component';


@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.scss']
})
export class TrackerComponent implements OnInit {
  specializations$: Observable<Specialization>;
  seniorities$: Observable<Seniority>;
  openSpecialization: Specialization;
  openSeniority: Seniority;
  openCompetency: Competency;

  displaySpecialization: Specialization;
  displaySeniority: Seniority;

  loadedGroups$: Observable<Group>;
  openGroup: Group;

  loadedTopics$: Observable<Topic>;

  constructor(public pdtApiService: PdtApiService,
              public dialog: MatDialog,
              private changeDetector: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.loadSpecializations();
  }

  refresh(){
    this.changeDetector.detectChanges();
  }

  loadSpecializations(){
    this.specializations$ = this.pdtApiService.getSpecializations();
  }

  loadSeniorities(specialization: Specialization){
    this.seniorities$ = null;
    this.seniorities$ = this.pdtApiService.getSeniorityBySpecialization(specialization.id);
    this.openSpecialization = specialization;
  }

  loadCompetencyGroups(specialization: Specialization, seniority: Seniority){
    let competency: Competency;
    this.openSeniority = seniority;
    this.openSpecialization = specialization;

    this.displaySeniority = seniority;
    this.displaySpecialization = specialization;

    this.openGroup = null;
    this.loadedTopics$ = null;

    this.pdtApiService.getCompetencyBySeniorityAndSpecialization(specialization.id, seniority.id)
    .pipe(take(1)).subscribe((data) => {
      competency = data;
      this.openCompetency = competency;

      this.loadedGroups$ = this.pdtApiService.getGroupsByCompetency(competency.id);
    });
  }

  loadTopics(group: Group){
    this.openGroup = group;
    this.loadedTopics$ = this.pdtApiService.getTopicsByGroup(group.id);
  }


  editSpecialization(event: Event, specialization: Specialization ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(EditorComponent, {
      width: '250px',
      data: { type: 'specialization', obj: specialization, mode: 'patch'}
    });
    dialogRef.componentInstance.dialogResult.subscribe(() => this.loadSpecializations());
  }

  addSpecialization(event: Event){
    event.stopPropagation();
    const newSpecialization: Specialization = {name: ''} as Specialization;
    const dialogRef = this.dialog.open(EditorComponent, {
      width: '250px',
      data: { type: 'specialization', obj: newSpecialization, mode: 'post'}
    });
    dialogRef.componentInstance.dialogResult.subscribe(() => this.loadSpecializations());
  }

  deleteSpecialization(event: Event, specialization: Specialization ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: `Do you really wish to delete ${specialization.name} specialization?`}
    });
    dialogRef.componentInstance.confirmed.subscribe(confirmed =>
    {
      if (confirmed)
      {
        this.pdtApiService.deleteSpecialization(specialization.id).subscribe(() => this.loadSpecializations());
      }
    });
  }

  addSeniority(event: Event){
    event.stopPropagation();
    const newSeniority: Seniority = {name: ''} as Seniority;
    const dialogRef = this.dialog.open(SeniorityChoiceDialogComponent,
      {
        width: '250px'
      });
    dialogRef.componentInstance.choice.subscribe((choice) => {
      this.pdtApiService.postCompetency(this.openSpecialization.id, choice.id).subscribe(
        () => this.loadSeniorities(this.openSpecialization)
        );
    });
  }

  addTopic(event: Event, group: Group){
    event.stopPropagation();
    const newTopic: Topic = {name: '', groupId: group.id} as Topic;
    const dialogRef = this.dialog.open(EditorComponent, {
      width: '250px',
      data: { type: 'topic', obj: newTopic, mode: 'post'}
    });
    dialogRef.componentInstance.dialogResult.subscribe(
      (res) => {
        this.pdtApiService.postCompetencyTopic(res.id, this.openCompetency.id).subscribe();
        this.loadTopics(this.openGroup);
      }
      );
  }

  addGroup(event: Event){
    event.stopPropagation();
    const newGroup: Group = {name: ''} as Group;
    const dialogRef = this.dialog.open(EditorComponent, {
      width: '250px',
      data: { type: 'group', obj: newGroup, mode: 'post'}
    });
    dialogRef.componentInstance.dialogResult.subscribe(
      () => {
        // testing bullshit (fix DB architecture to omit this horror)
        this.pdtApiService.postTopic('Start adding topics here', newGroup.id).subscribe(
          res => {
            this.pdtApiService.postCompetencyTopic(res.id, this.openCompetency.id).subscribe(
              () => {
                this.loadCompetencyGroups(this.openSpecialization, this.openSeniority);
              }
            );
          }
        );
      }
      );
  }

  deleteGroup(event: Event, group: Group ){
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: `Do you really wish to delete ${group.name} group?`}
    });
    dialogRef.componentInstance.confirmed.subscribe(confirmed =>
    {
      if (confirmed)
      {
        this.pdtApiService.deleteGroup(group.id).subscribe(() =>
        this.loadCompetencyGroups(this.openSpecialization, this.openSeniority));
      }
    });
  }

}
