import { Component, OnInit } from '@angular/core';
import { AuthService } from './../services/auth/auth.service';
import { of, Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {

  profile$: BehaviorSubject<any>;

  constructor(public auth: AuthService) {
  }

  // ngOnInit() {
  //   this.profile$ = this.auth.userProfile$;
  //   console.log("nav" + this.auth.userProfile);
  //   console.log("nav" + this.auth.userProfile$);

  // }
  ngOnInit() {
    if (this.auth.isAuthenticated() && !this.auth.userProfile$.value)
    {
      this.auth.getProfile();
    }
    this.profile$ = this.auth.userProfile$;
  }
}
