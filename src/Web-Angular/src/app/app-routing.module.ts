import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { UserComponent } from './user/user.component';
import { TrackerComponent } from './tracker/tracker.component';
import { AuthenticationGuard } from './guards/authentication.guard';


const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
              component: HomeComponent, },
    { path: 'tracker', loadChildren: () => import('./tracker/tracker.module').then(m => m.TrackerModule),
              component: TrackerComponent, canActivate: [AuthenticationGuard], },
    { path: 'userinfo', loadChildren: () => import('./user/user.module').then(m => m.UserModule),
              component: UserComponent, canActivate: [AuthenticationGuard], },
    { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
              component: ProfileComponent, canActivate: [AuthenticationGuard], },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
